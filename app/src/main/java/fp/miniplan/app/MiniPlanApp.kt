package fp.miniplan.app

import android.app.Application
import com.google.android.material.color.DynamicColors
import fp.miniplan.app.activity.main.messe.messeRepository
import fp.miniplan.app.activity.main.task.taskRepositoryModule
import fp.miniplan.app.module.apollo.apolloModule
import fp.miniplan.app.module.okhttp.okHttpClientModule
import fp.miniplan.app.module.preference.preferenceRepositoryModule
import fp.miniplan.app.module.retrofit.retrofitModule
import fp.miniplan.app.module.service.authServiceModule
import fp.miniplan.app.module.token.accessTokenProviderModule
import fp.miniplan.app.module.calendar.sync.CalendarSyncAdapterManager
import fp.miniplan.app.module.calendar.sync.calendarSyncAdapterModule
import fp.miniplan.app.module.viewmodel.base.viewModelModule
import org.acra.ReportField
import org.acra.config.httpSender
import org.acra.data.StringFormat
import org.acra.ktx.initAcra
import org.acra.sender.HttpSender
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MiniPlanApp : Application() {
    override fun onCreate() {
        super.onCreate()

        CalendarSyncAdapterManager.init(this@MiniPlanApp)
        DynamicColors.applyToActivitiesIfAvailable(this)

        startKoin {
            androidLogger()
            androidContext(this@MiniPlanApp)
            modules(
                preferenceRepositoryModule,
                accessTokenProviderModule,
                okHttpClientModule,
                apolloModule,
                taskRepositoryModule,
                calendarSyncAdapterModule,
                messeRepository,
                retrofitModule,
                authServiceModule,
                viewModelModule
            )
        }


        if (!BuildConfig.DEBUG) {
            initAcra {
                buildConfigClass = BuildConfig::class.java
                reportFormat = StringFormat.JSON

                httpSender {
                    uri = BuildConfig.CRASH_REPORT_ENDPOINT
                    basicAuthLogin = BuildConfig.CRASH_LOGIN
                    basicAuthPassword = BuildConfig.CRASH_PASSWORD
                    httpMethod = HttpSender.Method.POST
                }

                reportContent = listOf(
                    ReportField.ANDROID_VERSION,
                    ReportField.APP_VERSION_CODE,
                    ReportField.INSTALLATION_ID,
                    ReportField.PHONE_MODEL,
                    ReportField.BUILD_CONFIG,
                    ReportField.CUSTOM_DATA,
                    ReportField.STACK_TRACE,
                    ReportField.USER_CRASH_DATE,
                    ReportField.SHARED_PREFERENCES,
                )
            }
        }
    }
}