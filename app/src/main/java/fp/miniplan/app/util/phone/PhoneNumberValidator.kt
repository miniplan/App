package fp.miniplan.app.util.phone

import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import java.util.*

val phoneNumberUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance()

fun formatPhoneNumber(phoneNumber: String, countryCode: String = Locale.getDefault(Locale.Category.FORMAT).country): String? {
    return parsePhoneNumber(phoneNumber, countryCode)?.let {
        phoneNumberUtil.format(it, PhoneNumberUtil.PhoneNumberFormat.NATIONAL)
    }
}

fun parsePhoneNumber(
    phoneNumber: String, countryCode: String = Locale.getDefault(Locale.Category.FORMAT).country
): Phonenumber.PhoneNumber? {
    return runCatching { phoneNumberUtil.parse(phoneNumber, countryCode) }.getOrNull()
}

fun isValidPhoneNumber(
    phoneNumber: String,
    countryCode: String = Locale.getDefault(Locale.Category.FORMAT).country
) = parsePhoneNumber(phoneNumber, countryCode)?.let { phoneNumberUtil.isValidNumber(it) } ?: false