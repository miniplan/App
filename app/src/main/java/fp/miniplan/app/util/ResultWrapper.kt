package fp.miniplan.app.util

import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableStateFlow
import kotlin.experimental.ExperimentalTypeInference

sealed interface ResultWrapper<T> {
    class InitialState<T> : ResultWrapper<T>
    class Requested<T> : ResultWrapper<T>
    class Result<T>(val result: T?) : ResultWrapper<T>
}

suspend fun <T> FlowCollector<ResultWrapper<T>>.requested() {
    return emit(ResultWrapper.Requested())
}

suspend fun <T> FlowCollector<ResultWrapper<T>>.result(result: T) {
    return emit(ResultWrapper.Requested())
}

fun <T> initialStateFlow(): MutableStateFlow<ResultWrapper<T>> {
    return MutableStateFlow(ResultWrapper.InitialState())
}

@OptIn(ExperimentalTypeInference::class)
fun <T> flow(@BuilderInference block: suspend FlowCollector<ResultWrapper<T>>.() -> Unit){
    kotlinx.coroutines.flow.flow(block)
}

