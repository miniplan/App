package fp.miniplan.app.util

import android.app.Activity
import android.content.Context
import android.util.TypedValue
import android.view.View
import com.google.gson.*
import okhttp3.Response
import java.io.InputStreamReader

fun Activity.makeNotification(
    type: LowPriorityNotification.NotificationType,
    textId: Int,
    length: LowPriorityNotification.Length
) = LowPriorityNotification(this, type, textId, length)
