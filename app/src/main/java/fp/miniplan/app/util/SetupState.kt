package fp.miniplan.app.util

import android.app.Activity
import fe.android.preference.helper.OptionTypeMapper
import fp.miniplan.app.activity.login.LoginActivity
import fp.miniplan.app.activity.main.MainActivity
import kotlin.reflect.KClass

sealed class SetupState(val id: Int, val activity: KClass<out Activity>) {
    data object Login : SetupState(100, LoginActivity::class)
    data object Done : SetupState(200, MainActivity::class)

    companion object Companion : OptionTypeMapper<SetupState, Int>(
        { it.id },
        { arrayOf(Login, Done) }
    )
}