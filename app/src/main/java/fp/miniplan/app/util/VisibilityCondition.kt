package fp.miniplan.app.util

import android.view.View

class VisibilityCondition(visible: MutableList<View>, gone: MutableList<View>) {
    private val types = mapOf(ChangeState.Visible to visible, ChangeState.Gone to gone)

    fun update(condition: Boolean) {
        types.forEach { (state, list) ->
            list.forEach {
                state.applyState(it, condition)
            }
        }
    }

    enum class ChangeState {
        Visible {
            override fun applyState(view: View, condition: Boolean) {
                view.visibility = if (condition) View.VISIBLE else View.GONE
            }
        },
        Gone {
            override fun applyState(view: View, condition: Boolean) {
                view.visibility = if (condition) View.GONE else View.VISIBLE
            }
        };

        abstract fun applyState(view: View, condition: Boolean)
    }
}