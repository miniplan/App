package fp.miniplan.app.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import fp.miniplan.app.R
import fp.miniplan.app.job.TaskJobService

object NotificationHelper {
    fun createTaskJobNotificationChannel(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                TaskJobService.TASK_JOB_SERVICE_NOTIFICATION,
                context.getString(R.string.new_tasks_channel),
                NotificationManager.IMPORTANCE_DEFAULT
            ).apply {
                description = context.getString(R.string.new_task_channel_description)
            }


            context
                .getSystemService(NotificationManager::class.java)
                .createNotificationChannel(channel)
        }
    }
}