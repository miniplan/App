package fp.miniplan.app.util

import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

val timeFormatter: DateTimeFormatter = DateTimeFormat.forPattern("HH:mm")
val dateFormatter: DateTimeFormatter = DateTimeFormat.forPattern("EEEE, dd. MMMM yyyy")
val dayFormatter: DateTimeFormatter = DateTimeFormat.forPattern("EEEE")
val dateTimeFormatter: DateTimeFormatter =
    DateTimeFormat.forPattern("EEEE, dd. MMMM yyyy 'um' HH:mm 'Uhr'")