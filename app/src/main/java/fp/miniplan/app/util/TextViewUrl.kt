package fp.miniplan.app.util

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.text.Annotation
import android.text.SpannableString
import android.text.Spanned
import android.text.SpannedString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import fp.miniplan.app.R

object TextViewUrl {
    fun createClickable(
        context: Activity,
        stringId: Int,
        textView: TextView,
        annotationUrl: Map<String, String>,
        color: Int
    ) {
        textView.apply {
            text = createClickable(
                context, context.getText(stringId) as SpannedString, annotationUrl, color
            )
            movementMethod = LinkMovementMethod.getInstance()
        }
    }

    fun createClickable(
        context: Activity,
        privacyPolicyText: SpannedString,
        annotationUrl: Map<String, String>,
        color: Int,
    ): SpannableString {
        val spannableString = SpannableString(privacyPolicyText)
        val privacyAnnotations = privacyPolicyText.getSpans(
            0,
            privacyPolicyText.length,
            Annotation::class.java
        )

        annotationUrl.forEach { (linkName, url) ->
            privacyAnnotations?.find { it.value == linkName }?.let {
                spannableString.apply {
                    setSpan(
                        object : ClickableSpan() {
                            override fun onClick(widget: View) {
                                context.startActivity(Intent(Intent.ACTION_VIEW).apply {
                                    this.data = Uri.parse(url)
                                })
                            }
                        },
                        privacyPolicyText.getSpanStart(it),
                        privacyPolicyText.getSpanEnd(it),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                    setSpan(
                        ForegroundColorSpan(
                            color
                        ),
                        privacyPolicyText.getSpanStart(it),
                        privacyPolicyText.getSpanEnd(it),
                        0
                    )
                }
            }
        }

        return spannableString
    }
}