package fp.miniplan.app.util

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.children
import com.google.android.material.snackbar.Snackbar

class LowPriorityNotification(
    val activity: Activity,
    private val type: NotificationType,
    private val textId: Int,
    private val length: Length
) {

    fun show(){
        activity.runOnUiThread {
            type.show(activity, textId, length)
        }
    }

    enum class NotificationType {
        TOAST {
            override fun show(activity: Activity, textId: Int, length: Length) {
                Toast.makeText(activity, textId, length.toast).show()
            }
        },
        SNACKBAR {
            override fun show(activity: Activity, textId: Int, length: Length) {
                val group = activity.window.decorView as ViewGroup
                group.children.forEach {
                    println(it)
                    if(it is ViewGroup){
                        println(it.getChildAt(0))
                    }
                }

                //TODO: find a way to find the first view that can be properly used without putting snackbar under navbar
                Snackbar.make(
                    group.getChildAt(0) as View,
                    textId,
                    length.snackbar
                ).show()
            }
        };

        abstract fun show(activity: Activity, textId: Int, length: Length)
    }

    enum class Length(val toast: Int, val snackbar: Int) {
        SHORT(Toast.LENGTH_SHORT, Snackbar.LENGTH_SHORT),
        LONG(Toast.LENGTH_LONG, Snackbar.LENGTH_LONG);
    }
}