package fp.miniplan.app.util

import fp.miniplan.app.R

enum class TaskType(val taskName: String, val icon: Int) {
    AL("Altardiener", R.drawable.ic_task_al),
    AK("Akolyth", R.drawable.ic_task_ak),
    KR("Kreuz", R.drawable.ic_task_kr),
    TH("Thuriferar", R.drawable.ic_task_th),
    KA("Kaplan", R.drawable.ic_task_ka),
    MB("Messbuch", R.drawable.ic_task_mb),
    MI("Mikrofon", R.drawable.ic_task_mi),
    ZE("Zerimonier", R.drawable.ic_task_unknown),
    WW("Weihwasser", R.drawable.ic_task_unknown),
    KW("Kreuzweg", R.drawable.ic_task_kr),
    RA("Ratsche", R.drawable.ic_task_ratchet),
    SS("Speisesegnung", R.drawable.ic_food_blessing),
    OK("Osterkerze", R.drawable.ic_task_ak),
    UNKNOWN("Unbekannter Aufgabentyp", R.drawable.ic_task_unknown);

    companion object {
        fun safeValueOf(str: String?): TaskType {
            return values().find { it.name.equals(str, ignoreCase = true) } ?: UNKNOWN
        }
    }
}