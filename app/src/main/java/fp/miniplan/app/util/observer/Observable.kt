package fp.miniplan.app.util.observer

open class Observable<T>(initialVal: T) {

    var currentVal = initialVal

    private val dependants = mutableListOf<CustomObserver<T>>()

    fun set(newVal: T) {
        if (newVal != currentVal) {
            dependants.forEach {
                it.onChange(newVal, currentVal)
            }

            this.currentVal = newVal
        }
    }

    fun addObserver(observer: CustomObserver<T>, setCur: Boolean = false) {
        dependants.add(observer)
        if (setCur) {
            observer.onChange(currentVal, currentVal)
        }
    }

    fun removeObserver(observer: CustomObserver<T>) {
        dependants.remove(observer)
    }

    override fun toString(): String {
        return this.currentVal.toString()
    }
}