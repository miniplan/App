package fp.miniplan.app.util

import android.content.Context
import android.util.Log
import androidx.activity.result.ActivityResultCaller
import androidx.activity.result.contract.ActivityResultContracts
import fp.miniplan.app.ext.hasPermission
import org.koin.core.qualifier._q

typealias RequestPermissionHandler = () -> Unit
typealias OnPermissionGranted = () -> Unit

class PermissionHelper(activityResultCaller: ActivityResultCaller, val context: Context) {

    private var currentHandler: Pair<RequestPermissionHandler, OnPermissionGranted?>? = null

    private fun nextHandler(
        handler: RequestPermissionHandler,
        onPermissionGranted: OnPermissionGranted?
    ): Boolean {
        if (currentHandler != null) return false

        currentHandler = handler to onPermissionGranted
        return true
    }

    private val launcher = activityResultCaller.registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { result ->
        currentHandler?.let { handler ->
            if (result.values.all { it }) {
                val (action, onPermissionGranted) = handler
                onPermissionGranted?.invoke()
                action()
            }

            currentHandler = null
        }
    }


    fun runWithPermission(
        permissions: Array<String>,
        wasPermissionGranted: OnPermissionGranted?,
        fn: RequestPermissionHandler
    ): Boolean {
        val permissionsToRequest = permissions.filter { !context.hasPermission(it) }
        if (permissionsToRequest.isNotEmpty()) {
            if (!nextHandler(fn, wasPermissionGranted)) {
                return false
            }

            this.launcher.launch(permissionsToRequest.toTypedArray())
        } else fn()

        return true
    }
}