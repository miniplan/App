package fp.miniplan.app.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build


fun hasInternet(context: Context): Boolean {
    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val cap = cm.getNetworkCapabilities(cm.activeNetwork)
    return cap?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) ?: false && cap?.hasCapability(
        NetworkCapabilities.NET_CAPABILITY_VALIDATED
    ) ?: false
}