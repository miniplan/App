package fp.miniplan.app.util.observer

fun interface CustomObserver<T> {
    fun onChange(newVal: T, oldVal: T)
}