package fp.miniplan.app.util.observer

class ObservableBoolean(initVal: Boolean = false) : Observable<Boolean>(initVal)