package fp.miniplan.app.ext

import android.util.Log
import retrofit2.Call

typealias OnError = (errorMsg: Throwable) -> Unit

class RequestException(message: String) : Exception(message)

fun <T> Call<T>.executeHandleErrors(error: Pair<OnError, String>) =
    executeHandleErrors(error.first, error.second)

fun <T> Call<T>.executeHandleErrors(onError: OnError, defaultError: String): T? {
    return kotlin.runCatching {
        val resp = this.execute()
        Log.d("CallExecutor", "${resp.code()}: ${this.request().method} ${this.request().url}")
        if (!resp.isSuccessful) {
            throw RequestException(resp.errorBody()?.string().also {
                Log.d("CallExecutor", "Error body: $it")
            } ?: defaultError.also {
                Log.d("CallExecutor", "Default error: $it")
            })
        }

        resp.body().also {
            Log.d("CallExecutor", "Success: $it")
        }
    }.onFailure(onError).getOrNull()
}