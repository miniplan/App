package fp.miniplan.app.ext

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi

fun <T> Deferred<T?>.onNonNullResult(fn: (T) -> Unit) {
    this.onResult { if (it != null) fn(it) }

}

fun <T> Deferred<T?>.onNullResult(fn: (T?) -> Unit) {
    this.onResult { if (it == null) fn(it) }
}


@OptIn(ExperimentalCoroutinesApi::class)
fun <T> Deferred<T?>.onResult(fn: (T?) -> Unit) {
    this.invokeOnCompletion {
        fn(this.getCompleted())
    }
}
