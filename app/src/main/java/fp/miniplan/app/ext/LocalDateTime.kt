package fp.miniplan.app.ext

import org.joda.time.LocalDateTime

fun getWeekStart(): LocalDateTime {
    return LocalDateTime.now()
        .withDayOfWeek(1)
        .withTime(0, 0,0,0)
}
