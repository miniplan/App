package fp.miniplan.app.ext

import org.joda.time.LocalDateTime
import java.util.*

fun Any.asUUID(): UUID {
    return if(this is String){
        UUID.fromString(this)
    } else throw IllegalArgumentException("Any is not a string")
}

fun Any?.toLocalDate() =
    (this as? String)?.let { LocalDateTime.parse(it) } ?: LocalDateTime(0L)