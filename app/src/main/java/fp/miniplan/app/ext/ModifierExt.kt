package fp.miniplan.app.ext

import androidx.compose.ui.Modifier

fun Modifier.ifTrueFalse(
    bool: Boolean,
    runTrue: (Modifier) -> Modifier,
    runFalse: (Modifier) -> Modifier
): Modifier {
    return if (bool) runTrue(this) else runFalse(this)
}

fun Modifier.ifTrue(bool: Boolean, run: (Modifier) -> Modifier): Modifier {
    return if (bool) run(this) else this
}