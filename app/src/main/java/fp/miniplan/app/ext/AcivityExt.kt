package fp.miniplan.app.ext

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import fe.android.preference.helper.BasePreference
import fe.android.preference.helper.compose.RepositoryState
import fp.miniplan.app.module.preference.Preferences
import fp.miniplan.app.util.SetupState
import kotlin.reflect.KClass

fun <T : Activity> Activity.changeActivity(
    activity: KClass<T>,
    bundle: Bundle? = null,
    flags: List<Int> = emptyList(),
    finish: Boolean = true
) {
    return this.startActivity(Intent(this, activity.java).apply {
        if (bundle != null) this.putExtras(bundle)
        flags.forEach { this.addFlags(it) }
    }).also {
        if (finish) this.finish()
    }
}


fun <S : SetupState> Activity.changeActivity(setupState: S, bundle: Bundle? = null) {
    return this.changeActivity(setupState.activity, bundle, finish = true)
}

fun <S : SetupState> Activity.changeAndSaveActivity(
    preference: RepositoryState<SetupState, SetupState, BasePreference.MappedPreference<SetupState, Int>>,
    setupState: S,
    bundle: Bundle? = null,
) {
    preference.updateState(setupState)
    return this.changeActivity(setupState, bundle)
}