package fp.miniplan.app.ext

import fp.miniplan.fragment.MinistrantFragment

fun MinistrantFragment.isMatch(searchText: String): Boolean {
    return this.firstname.contains(searchText, ignoreCase = true) || this.lastname.contains(searchText, ignoreCase = false)
}