package fp.miniplan.app.ui.composable

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp

@Composable
fun IconTextButtonContent(
    @StringRes text: Int,
    iconVector: ImageVector,
) {
    Icon(iconVector, contentDescription = null, modifier = Modifier.size(17.dp))
    Spacer(modifier = Modifier.width(2.dp))
    Text(stringResource(id = text))
}