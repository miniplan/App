package fp.miniplan.app.ui.composable

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Switch
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import fe.android.preference.helper.BasePreference
import fe.android.preference.helper.compose.RepositoryState


enum class BooleanPreferenceType {
    Checkbox,
    Switch;

    companion object {
        val Default = Switch
    }
}

@Composable
fun BooleanPreference(
    state: RepositoryState<Boolean, Boolean, BasePreference.Preference<Boolean>>,
    disabled: Boolean = false,
    stateType: BooleanPreferenceType = BooleanPreferenceType.Default,
    onCheckedChange: (Boolean) -> Unit,
    slot: @Composable () -> Unit
) {
    ClickableRow(
        onClick = { if (!disabled) onCheckedChange(!state.value) },
        disabled = disabled
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .weight(5f)
        ) {
            slot()
        }

        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .weight(0.5f)
        )

        when (stateType) {
            BooleanPreferenceType.Checkbox -> {
                Checkbox(
                    checked = state.value,
                    onCheckedChange = if (!disabled) onCheckedChange else null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f)
                )
            }

            BooleanPreferenceType.Switch -> {
                Switch(
                    checked = state.value,
                    onCheckedChange = if (!disabled) onCheckedChange else null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f)
                )
            }
        }
    }
}

@Composable
fun BooleanPreference(
    state: RepositoryState<Boolean, Boolean, BasePreference.Preference<Boolean>>,
    disabled: Boolean = false,
    stateType: BooleanPreferenceType = BooleanPreferenceType.Default,
    slot: @Composable () -> Unit
) {
    BooleanPreference(state, disabled, stateType, { state.updateState(it) }, slot)
}

//@Composable
//fun BooleanPreference(
//    statePreference: PreferenceObservable<Boolean>,
//    disabled: Boolean = false,
//    stateType: BooleanPreferenceType = BooleanPreferenceType.Default,
//    slot: @Composable () -> Unit
//) {
//    val state by statePreference.observeAsState()
//    BooleanPreference(state, disabled, stateType, onCheckedChange = {
//        statePreference.updateValue(!state)
//    }, slot)
//}

