package fp.miniplan.app.ui.composable

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import fp.miniplan.app.ext.ifTrueFalse


@Composable
fun ClickableColumn(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    disabled: Boolean = false,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = modifier.clickable(disabled, onClick)
    ) {
        content(this)
    }
}

@Composable
fun ClickableRow(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    disabled: Boolean = false,
    content: @Composable RowScope.() -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier.clickable(disabled, onClick)
    ) {
        content(this)
    }
}

private fun Modifier.clickable(
    disabled: Boolean = false,
    onClick: () -> Unit
): Modifier {
    return this
        .clip(RoundedCornerShape(8.dp))
        .ifTrueFalse(disabled, runTrue = { it.alpha(0.3F) }, runFalse = {
            it.clickable {
                onClick()
            }
        })
        .padding(6.dp)
}