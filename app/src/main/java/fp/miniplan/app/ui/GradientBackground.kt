package fp.miniplan.app.ui

import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.DrawScope
import kotlin.math.*

@Composable
fun MaterialTheme.gradientColors(alpha: Float) = listOf(
    this.colorScheme.primary.copy(alpha = alpha),
    this.colorScheme.secondary.copy(alpha = alpha),
    this.colorScheme.tertiary.copy(alpha = alpha)
)

//https://www.geeksforgeeks.org/angled-gradient-background-in-android-using-jetpack-compose/
fun gradientBackground(colors: List<Color>, angle: Float): DrawScope.() -> Unit = {
    // Setting the angle in radians
    val angleRad = angle / 180f * Math.PI

    // Fractional x
    val x = cos(angleRad).toFloat()

    // Fractional y
    val y = sin(angleRad).toFloat()

    // Set the Radius and offset as shown below
    val radius = sqrt(size.width.pow(2) + size.height.pow(2)) / 2f
    val offset = center + Offset(x * radius, y * radius)

    // Setting the exact offset
    val exactOffset = Offset(
        x = min(offset.x.coerceAtLeast(0f), size.width),
        y = size.height - min(offset.y.coerceAtLeast(0f), size.height)
    )

    // Draw a rectangle with the above values
    drawRect(
        brush = Brush.linearGradient(
            colors = colors,
            start = Offset(size.width, size.height) - exactOffset,
            end = exactOffset
        ),
        size = size
    )
}
