package fp.miniplan.app.ui

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.relocation.BringIntoViewRequester
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@OptIn(ExperimentalFoundationApi::class)
fun Modifier.bringIntoView(
    coroutineScope: CoroutineScope,
    bringIntoViewRequester: BringIntoViewRequester
): Modifier {
    return this.onFocusEvent { focusState ->
        if (focusState.isFocused) coroutineScope.launch { bringIntoViewRequester.bringIntoView() }
    }
}