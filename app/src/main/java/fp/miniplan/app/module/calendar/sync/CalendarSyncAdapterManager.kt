package fp.miniplan.app.module.calendar.sync

import android.accounts.Account
import android.accounts.AccountManager
import android.app.AlarmManager
import android.content.ContentResolver
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.core.content.getSystemService

object CalendarSyncAdapterManager {
    private const val authority = "com.android.calendar"
    const val accountType = "fp.miniplan.app.account"
    const val accountName = "MiniPlan"
    private var account = Account(accountName, accountType)

    const val addToCalendarId = "add_to_calendar"
    const val reminderMinutesId = "reminder_minutes"

    fun init(context: Context) {
        ContentResolver.setSyncAutomatically(account, authority, true)
        ContentResolver.setIsSyncable(account, accountType, 1)

        // add periodic sync interval once per day
        ContentResolver.addPeriodicSync(account, accountType, Bundle(), AlarmManager.INTERVAL_DAY)

        val accountManager = context.getSystemService<AccountManager>()
        if (accountManager?.addAccountExplicitly(account, null, null) == true) {
            Log.d("SyncAdapterManager", "Account added")
        } else {
            Log.d("SyncAdapterManager", "Account has already been added")
        }

        ContentResolver.setSyncAutomatically(account, authority, true)
    }

    fun forceRefresh(addToCalendar: Boolean, reminderMinutes: Int) {
        Log.d("SyncAdapterManager", "Requesting force sync")

        ContentResolver.requestSync(account, authority, Bundle().apply {
            putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true)
            putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true)
            putBoolean(addToCalendarId, addToCalendar)
            putInt(reminderMinutesId, reminderMinutes)
        })
    }
}
