package fp.miniplan.app.module.viewmodel

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.viewModelScope
import com.apollographql.apollo3.exception.ApolloException
import fe.android.preference.helper.PreferenceRepository
import fp.miniplan.GetMessenAfterQuery
import fp.miniplan.app.activity.main.messe.MesseRepository
import fp.miniplan.app.module.viewmodel.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import org.joda.time.LocalDateTime
import org.koin.core.component.KoinComponent

class MesseViewModel(
    preferenceRepository: PreferenceRepository,
    val messeRepository: MesseRepository,
) : BaseViewModel(preferenceRepository), KoinComponent {

    var messen = mutableStateListOf<GetMessenAfterQuery.Messe>()
        private set

    init {
        fetchMessenAsync()
    }

    fun fetchMessenAsync(
        after: LocalDateTime = LocalDateTime.now()
    ) = viewModelScope.async(Dispatchers.IO) {
        val data = try {
            messeRepository.fetchMessen(after)
        } catch (e: ApolloException) {
            e.printStackTrace()
            null
        }

        data?.Messe?.let {
            messen.clear()
            messen.addAll(it)
        }
    }
}