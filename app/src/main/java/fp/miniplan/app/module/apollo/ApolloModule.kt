package fp.miniplan.app.module.apollo

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.cache.normalized.normalizedCache
import com.apollographql.apollo3.cache.normalized.sql.SqlNormalizedCacheFactory
import com.apollographql.apollo3.network.okHttpClient
import fp.miniplan.app.BuildConfig
import org.koin.dsl.module

val apolloModule = module {
    single {
        val sqlNormalizedCacheFactory = SqlNormalizedCacheFactory(get(), "apollo.db")

        ApolloClient.Builder()
            .serverUrl(BuildConfig.API_HOST)
            .normalizedCache(sqlNormalizedCacheFactory)
            .okHttpClient(get())
            .build()

    }
}