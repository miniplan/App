package fp.miniplan.app.module.okhttp

import android.util.Log
import fp.miniplan.app.module.token.AccessTokenProvider
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import org.koin.dsl.module
import java.io.IOException


val okHttpClientV2Module = module {
    single {
        val accessTokenProvider = get<AccessTokenProvider>()

        OkHttpClient().newBuilder()
            .addInterceptor(MiniPlanAccessTokenInterceptor(accessTokenProvider))
            .build()
    }
}

class MiniPlanAccessTokenInterceptor(
    private val accessTokenProvider: AccessTokenProvider
) : Interceptor {
    companion object {

//        {"error":{"code":"invalid-jwt","error":"Could not verify JWT: JWTExpired","path":"$"
//        {"errors":[{"extensions":{"path":"$","code":"invalid-jwt"},"message":"Could not verify JWT: JWTExpired"}]}
//        {"errors":[{"extensions":{"code":"invalid-jwt","path":"$"},"message":"Could not verify JWT: JWTExpired"}]}

        private val jwtErrors = listOf(
            "{\"errors\":[{\"extensions\":{\"path\":\"\$\",\"code\":\"invalid-jwt\"},\"message\":\"Could not verify JWT: JWTExpired\"}]}".toByteArray(),
            "{\"errors\":[{\"extensions\":{\"code\":\"invalid-jwt\",\"path\":\"\$\"},\"message\":\"Could not verify JWT: JWTExpired\"}]}".toByteArray(),
        )
        val expiredTokens = listOf("invalid-jwt", "JWTExpired")
        val peekBytes = jwtErrors.maxOf { it.size.toLong() }
    }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val token = accessTokenProvider.readAuthToken()!!
        Log.d("Interceptor", "Token is $token")

        val builder = chain.request().newBuilder()

//        token?.let {
        //        }

//        val response = try {
//            chain.proceed(builder.build())
//        } catch (e: Throwable) {
//            throw IOException(e.message)
//        }
//
//        Log.d("Interceptor", "Request response is ${response.code} for ${response.request.url}")
//        Log.d("Interceptor", "Body: ${response.peekBody(100000000).string()}")
//
//        val bodyCopy = response.peekBody(peekBytes).string()
//        if (expiredTokens.all { bodyCopy.contains(it) }) {
//            synchronized(this) {
//                val currentToken = accessTokenProvider.readAccessToken()
//                val newToken = if (currentToken != null && currentToken == token) {
//                    accessTokenProvider.refreshAccessToken()
//                } else null
//
//                Log.d("Interceptor", "New token is $newToken")
//
//                if (newToken != null) {
//                    response.close()
//                    return chain.proceed(builder.setAuthorization(newToken).build())
//                }
//            }
//        }
        return chain.proceed(builder.setAuthorization(token).build())
    }

    private fun Request.Builder.setAuthorization(accessToken: String): Request.Builder {
        return this.header("Authorization", "Bearer $accessToken")
    }
}

val jsonMediaTypeV2 = "application/json".toMediaTypeOrNull()


class AccessTokenInterceptorV2(
    private val accessTokenProvider: AccessTokenProvider
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        Log.d("AccessTokenInterceptor", "intercepting")
        val token = accessTokenProvider.readAccessToken()
        Log.d("AccessTokenInterceptor", "Accesstoken=$token")


        //hasura returns 400 on /v1alpha1/ and 200 on /v1/, rewrite 400 -> 401 so authenticator gets triggered
        val req = with(chain.request()) {
            if (token != null) {
                this.newBuilder()
                    .addHeader("Authorization", "Bearer $token")
                    .build()
            } else this
        }

        val resp = chain.proceed(req)
        return if (resp.code == 400) {
            resp.newBuilder().code(401).build()
        } else resp
    }
}

class AccessTokenAuthenticatorV2(
    private val accessTokenProvider: AccessTokenProvider
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        Log.d("AccessTokenAuthenticator", "authenticate")
        val accessToken = accessTokenProvider.readAccessToken() ?: return null
        if (!isRequestWithAccessToken(response)) {
            return null
        }

        Log.d("fp.miniplan.app.util.calendar.Authenticator", "Is request with token")


        synchronized(this) {
            val newAccessToken = accessTokenProvider.readAccessToken() ?: return null
            Log.d(
                "fp.miniplan.app.util.calendar.Authenticator",
                "New token != old token: ${accessToken != newAccessToken}"
            )

            if (accessToken != newAccessToken) {
                return newRequestWithAccessToken(response.request, newAccessToken)
            }

            Log.d("fp.miniplan.app.util.calendar.Authenticator", "Refreshing token..")

            val updatedAccessToken = accessTokenProvider.refreshAccessToken() ?: return null
            return newRequestWithAccessToken(response.request, updatedAccessToken)
        }
    }

    private fun isRequestWithAccessToken(response: Response): Boolean {
        val header = response.request.header("Authorization")
        return header != null && header.startsWith("Bearer")
    }

    private fun newRequestWithAccessToken(request: Request, accessToken: String): Request {
        return request.newBuilder()
            .header("Authorization", "Bearer $accessToken")
            .build()
    }
}