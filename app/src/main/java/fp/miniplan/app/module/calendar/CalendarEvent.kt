package fp.miniplan.app.module.calendar

import android.content.*
import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract
import fp.miniplan.fragment.TaskFragment
import org.joda.time.LocalDateTime
import java.util.*

class CalendarEvent(task: TaskFragment, localDateTime: LocalDateTime) {
    private val month = localDateTime.monthOfYear - 1
    private var title = "Ministrieren: ${task.taskTypeRel?.name}"
    private var startMillis = getMillis(localDateTime)
    private var endMillis = getMillis(localDateTime, task.duration)

    private fun getMillis(localDateTime: LocalDateTime, offset: Int = 0): Long {
        calendar.set(
            localDateTime.year,
            month,
            localDateTime.dayOfMonth,
            localDateTime.hourOfDay + offset,
            localDateTime.minuteOfHour
        )
        return calendar.timeInMillis
    }

    fun getViewIntent(cursor: Cursor): Intent {
        cursor.moveToFirst()

        val uri = ContentUris.withAppendedId(
            CalendarContract.Events.CONTENT_URI,
            cursor.getLong(3)
        )

        return Intent(Intent.ACTION_VIEW).setData(uri).also {
            cursor.close()
        }
    }

    @Throws(ActivityNotFoundException::class)
    fun getCreateIntent(name: String? = null): Intent {
        return Intent(Intent.ACTION_INSERT).apply {
            this.data = CalendarContract.Events.CONTENT_URI
            this.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
            this.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
            this.putExtra(CalendarContract.Events.TITLE, buildTitle(name))
            this.putExtra(CalendarContract.Events.EVENT_LOCATION, location)
        }
    }

    private fun buildTitle(name: String?) = buildString {
        this.append(title)
        if (name != null) {
            this.append(" (").append(name).append(")")
        }
    }

    fun buildEvent(uri: Uri, calendarId: Long, name: String? = null): ContentProviderOperation {
        return ContentProviderOperation.newInsert(uri).apply {
            withValue(CalendarContract.Events.CALENDAR_ID, calendarId)
            withValue(CalendarContract.Events.DTSTART, startMillis)
            withValue(CalendarContract.Events.DTEND, endMillis)
            withValue(CalendarContract.Events.TITLE, buildTitle(name))
            withValue(CalendarContract.Events.STATUS, CalendarContract.Events.STATUS_CONFIRMED)
            withValue(CalendarContract.Events.HAS_ALARM, 1)
        }.build()
    }

    fun getCalendarCursor(contentResolver: ContentResolver): Cursor {
        val proj = arrayOf(
            CalendarContract.Instances._ID,
            CalendarContract.Instances.BEGIN,
            CalendarContract.Instances.END,
            CalendarContract.Instances.EVENT_ID
        )

        return CalendarContract.Instances.query(
            contentResolver,
            proj,
            startMillis,
            endMillis,
            "\"$title\""
        )
    }

//    override fun toString(): String {
//        return "CalendarEvent(title='$title', localDateTime=$localDateTime)"
//    }

    companion object {
        val calendar: Calendar = Calendar.getInstance()
        const val location = "Seckau"
    }
}