package fp.miniplan.app.module.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.viewModelScope
import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.cache.normalized.api.NormalizedCache
import com.apollographql.apollo3.cache.normalized.apolloStore
import fe.android.preference.helper.PreferenceRepository
import fe.android.preference.helper.compose.getBooleanState
import fe.android.preference.helper.compose.getIntState
import fe.android.preference.helper.compose.getStringState
import fp.miniplan.app.module.preference.Preferences
import fp.miniplan.app.module.viewmodel.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SettingsViewModel(
    val context: Application,
    private val apolloClient: ApolloClient,
    preferenceRepository: PreferenceRepository
) : BaseViewModel(preferenceRepository) {

    val fullName = preferenceRepository.getStringState(Preferences.fullName)
    val phoneNumber = preferenceRepository.getStringState(Preferences.phoneNumber)
    val notifyOnNewTasksAvailable =
        preferenceRepository.getBooleanState(Preferences.notifyOnNewTasksAvailable)
    val automaticallyAddTasksToCalendar =
        preferenceRepository.getBooleanState(Preferences.automaticallyAddTasksToCalendar)
    val taskReminderTime = preferenceRepository.getIntState(Preferences.taskReminderTime)
    val crashLogsEnabled = preferenceRepository.getBooleanState(Preferences.crashLogsEnabled)

    fun dropCache() = viewModelScope.launch(Dispatchers.IO) {
        apolloClient.apolloStore.clearAll()
    }


    fun debugCache() = viewModelScope.launch(Dispatchers.IO) {
        Log.d("SettingsViewModel", NormalizedCache.prettifyDump(apolloClient.apolloStore.dump()))
    }
}