package fp.miniplan.app.module.viewmodel

import android.util.Log
import android.widget.FrameLayout
import androidx.compose.runtime.*
import androidx.lifecycle.viewModelScope
import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Optional
import com.apollographql.apollo3.cache.normalized.FetchPolicy
import com.apollographql.apollo3.cache.normalized.emitCacheMisses
import com.apollographql.apollo3.cache.normalized.fetchPolicy
import com.apollographql.apollo3.exception.ApolloException
import com.google.android.material.bottomsheet.BottomSheetBehavior
import fe.android.preference.helper.PreferenceRepository
import fe.android.preference.helper.compose.getBooleanState
import fe.android.preference.helper.compose.getIntState
import fp.miniplan.GetMinistrantenSetupListQuery
import fp.miniplan.InsertDeleteUserMinistrantMutation
import fp.miniplan.app.ext.asUUID
import fp.miniplan.app.module.preference.Preferences
import fp.miniplan.app.module.viewmodel.base.BaseViewModel
import fp.miniplan.app.util.ResultWrapper
import fp.miniplan.fragment.MinistrantFragment
import fp.miniplan.type.UserMinistrant_insert_input
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import java.util.*


class SetupViewModel(
    private val apolloClient: ApolloClient,
    preferenceRepository: PreferenceRepository
) : BaseViewModel(preferenceRepository), KoinComponent {

    val automaticallyAddTasksToCalendar = preferenceRepository.getBooleanState(
        Preferences.automaticallyAddTasksToCalendar
    )

    val taskReminderTime = preferenceRepository.getIntState(Preferences.taskReminderTime)

    var setupData by mutableStateOf<ResultWrapper<SetupData>>(ResultWrapper.InitialState())
        private set

    var selectedMinistranten = mutableStateListOf<UUID>()
        private set

    var selectorAtTop by mutableStateOf(true)
    var allowDrag by mutableStateOf(false)


    var behavior: BottomSheetBehavior<FrameLayout>? = null

    data class SetupData(
        val user: MinistrantFragment?,
        //pagination?
        val ministranten: List<MinistrantFragment>,
        val selectedMinistranten: List<MinistrantFragment>,
    )

    init {
        fetchSetupDataAsync()
    }

    fun fetchSetupDataAsync(): Deferred<Unit> {
        setupData = ResultWrapper.Requested()
        return viewModelScope.async(Dispatchers.IO) {
            Log.d("Viewmodel", "Launching fetch")

            apolloClient.query(GetMinistrantenSetupListQuery())
                .fetchPolicy(FetchPolicy.NetworkOnly)
                .emitCacheMisses(false)
                .execute().data?.let { data ->
                    Log.d("ViewModel", "Data received=$data")
                    val result = SetupData(
                        data.User.firstOrNull()?.ministrantRel?.ministrantFragment,
                        data.Ministrant.map { it.ministrantFragment },
                        data.UserMinistrant.map { it.ministrantRel.ministrantFragment })

                    Log.d("ViewModel", "Minis=${result.ministranten}")

                    setupData = ResultWrapper.Result(result)
                    selectedMinistranten = result.selectedMinistranten.map { it.id.asUUID() }
//                        .toMutableList()
//                        .also {
//                            if (result.user != null && !it.contains(result.user.id)) {
//                                it.add(result.user.id)
//                            }
//                        }
                        .toMutableStateList()
                }
        }
    }

    suspend fun updateSelection(added: Set<UUID>, deleted: Set<UUID>): Pair<Int?, Int?> {
//        val userId = preferenceRepository.getString(PreferenceRepository.userId)
//        Log.d("SetupViewModel", "Userid = $userId")
        return withContext(Dispatchers.IO) {
            val query = InsertDeleteUserMinistrantMutation(
                deleteMinistrant = Optional.presentIfNotNull(deleted.map { it.toString() }.toList()),
                insertMinistrant = added.map {
                    UserMinistrant_insert_input(
                        ministrant = Optional.presentIfNotNull(it.toString()),
//                        user = Optional.presentIfNotNull(userId)
                    )
                }
            )

            try {
                val data = apolloClient.mutation(query).execute().data
                data?.insert_UserMinistrant?.affected_rows to data?.delete_UserMinistrant?.affected_rows
            } catch (e: ApolloException) {
                e.printStackTrace()
                null to null
            }
        }
    }

    fun updateSelectionAsync(added: Set<UUID>, deleted: Set<UUID>): Deferred<Pair<Int?, Int?>> {
        Log.d("UpdateSelection", "added=$added, deleted=$deleted")
        return viewModelScope.async(Dispatchers.IO) {
            updateSelection(added, deleted)
        }
    }
}
