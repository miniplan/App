package fp.miniplan.app.module.token

import androidx.annotation.Keep
import com.auth0.jwt.JWT
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.annotations.SerializedName
import fe.android.preference.helper.PreferenceRepository
import fe.gson.extension.json.`object`.asObject
import fe.gson.extension.json.`object`.asString
import fp.miniplan.app.BuildConfig
import fp.miniplan.app.module.preference.Preferences
import fp.miniplan.app.module.service.AuthService
import okhttp3.OkHttpClient
import okhttp3.Request
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import java.util.*

val accessTokenProviderModule = module {
    singleOf(::AccessTokenProvider)
}

class AccessTokenProvider(
    private val preferences: PreferenceRepository
) {
    private val okHttpClient = OkHttpClient()
    private val gson = Gson()

    fun hasAccessTokenExpired(): Boolean {
        return hasExpired(preferences.getString(Preferences.jwtAccessToken))
    }

    fun hasRefreshTokenExpired(): Boolean {
        return hasExpired(preferences.getString(Preferences.jwtRefreshToken))
    }

    fun hasExpired(token: String?): Boolean {
        if (token == null) return true

        val decodedJwt = JWT.decode(token)
        val exp = decodedJwt.getClaim("exp")?.asLong() ?: 0

        return exp < System.currentTimeMillis() / 1000
    }

    fun readAuthToken() = preferences.getString(Preferences.authToken)

    fun readAccessToken() = preferences.getString(Preferences.jwtAccessToken)

    fun refreshAccessToken(): String? {
        val refreshToken = preferences.getString(Preferences.jwtRefreshToken)
        val request = Request.Builder()
            .addHeader("Authorization", "Bearer $refreshToken")
            .url("${BuildConfig.AUTH_HOST}/refresh")
            .build()

        val resp = okHttpClient.newCall(request).execute()
        return if (resp.code == 200) {
            val tokens = gson.fromJson(resp.body?.string(), NewTokensResponse::class.java)

            preferences.defaultEditor {
                preferences.writeString(Preferences.jwtAccessToken, tokens.newAccessToken, this)
                preferences.writeString(Preferences.jwtRefreshToken, tokens.newRefreshToken, this)
            }

            tokens.newAccessToken
        } else null
    }

    fun pickupJwt(token: String): Boolean {
        val request = Request.Builder().url("${BuildConfig.AUTH_HOST}/pickupjwt/$token").build()
        val resp = okHttpClient.newCall(request).execute()
        if (resp.code == 200) {
            val obj = JsonParser.parseString(resp.body?.string()) as JsonObject
            val ui = obj.asObject("userInfo")

            val info = AuthService.UserInfo(
                ui.asString("firstname"),
                ui.asString("lastname"),
                ui.asString("phoneNumber"),
                UUID.fromString(ui.asString("userId"))
            )

            this.store(obj.asString("accessToken"), obj.asString("refreshToken"), info)
            return true
        }

        return false
    }

    @Keep
    data class NewTokensResponse(
        @SerializedName("newAccessToken") val newAccessToken: String,
        @SerializedName("newRefreshToken") val newRefreshToken: String,
    )

    fun store(authToken: String, userInfo: AuthService.UserInfo){
        preferences.defaultEditor {
            preferences.writeString(Preferences.authToken, authToken, this)
            preferences.writeString(Preferences.userId, userInfo.userId.toString(), this)
            preferences.writeString(
                Preferences.fullName,
                "${userInfo.firstname} ${userInfo.lastname}", this
            )
            preferences.writeString(Preferences.phoneNumber, userInfo.phoneNumber, this)
        }
    }

    fun store(accessToken: String, refreshToken: String, userInfo: AuthService.UserInfo) {
        preferences.defaultEditor {
            preferences.writeString(Preferences.jwtAccessToken, accessToken, this)
            preferences.writeString(Preferences.jwtRefreshToken, refreshToken, this)
            preferences.writeString(Preferences.userId, userInfo.userId.toString(), this)
            preferences.writeString(
                Preferences.fullName,
                "${userInfo.firstname} ${userInfo.lastname}", this
            )
            preferences.writeString(Preferences.phoneNumber, userInfo.phoneNumber, this)
        }
    }
}