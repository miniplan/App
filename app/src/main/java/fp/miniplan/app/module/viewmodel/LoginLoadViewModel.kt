package fp.miniplan.app.module.viewmodel

import androidx.lifecycle.viewModelScope
import fe.android.preference.helper.PreferenceRepository
import fp.miniplan.app.module.token.AccessTokenProvider
import fp.miniplan.app.module.viewmodel.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import org.koin.core.component.KoinComponent

class LoginLoadViewModel(
    private val accessTokenProvider: AccessTokenProvider,
    preferenceRepository: PreferenceRepository
) : BaseViewModel(preferenceRepository), KoinComponent {

    fun getJWTAsync(token: String) = viewModelScope.async(Dispatchers.IO) {
        accessTokenProvider.pickupJwt(token)
    }
}