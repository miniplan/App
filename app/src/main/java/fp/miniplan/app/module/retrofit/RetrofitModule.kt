package fp.miniplan.app.module.retrofit

import fp.miniplan.app.BuildConfig
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

val retrofitModule = module {
    single {
        Retrofit.Builder()
            .baseUrl(BuildConfig.AUTH_HOST)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create()).build()
    }
}