package fp.miniplan.app.module.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.work.WorkManager
import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Optional
import com.apollographql.apollo3.cache.normalized.FetchPolicy
import com.apollographql.apollo3.cache.normalized.apolloStore
import com.apollographql.apollo3.cache.normalized.fetchPolicy
import com.apollographql.apollo3.exception.ApolloException
import fe.android.preference.helper.PreferenceRepository
import fe.android.preference.helper.compose.getBooleanState
import fe.android.preference.helper.compose.getIntState
import fe.android.preference.helper.compose.getState
import fe.android.preference.helper.compose.getStringState
import fp.miniplan.GetChangelogQuery
import fp.miniplan.GetMesseByIdQuery
import fp.miniplan.app.BuildConfig
import fp.miniplan.app.activity.main.task.TaskRepository
import fp.miniplan.app.module.calendar.CalendarEvent
import fp.miniplan.app.ext.getWeekStart
import fp.miniplan.app.ext.toLocalDate
import fp.miniplan.app.module.preference.Preferences
import fp.miniplan.app.module.calendar.sync.CalendarSyncWorker
import fp.miniplan.app.module.viewmodel.base.BaseViewModel
import fp.miniplan.fragment.MinistrantFragment
import fp.miniplan.fragment.TaskFragment
import kotlinx.coroutines.*
import org.joda.time.LocalDateTime
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.*

class MainViewModel(
    val app: Application,
    private val apolloClient: ApolloClient,
    private val taskRepository: TaskRepository,
    private val preferenceRepository: PreferenceRepository
) : BaseViewModel(preferenceRepository) {
    val setupState = preferenceRepository.getState(Preferences.setupState)
    val lastLaunchedVersion = preferenceRepository.getIntState(Preferences.lastLaunchedVersion)
    val inviteToken = preferenceRepository.getStringState(Preferences.inviteToken)

    private val automaticallyAddTasksToCalendar = preferenceRepository.getBooleanState(Preferences.automaticallyAddTasksToCalendar)
    private val taskReminderTime = preferenceRepository.getIntState(Preferences.taskReminderTime)

    val selectedMinistrants by lazy { MutableLiveData(0) }
    val taskList by lazy { MutableLiveData<List<Task>>() }

    init {
        Log.d("MainViewModel", "Init")
        fetchTasksAsync()
    }

    fun logoutAsync() = viewModelScope.launch(Dispatchers.IO) {
        preferenceRepository.defaultEditor {
            Preferences.all.forEach { remove(it.key) }
        }
        apolloClient.apolloStore.clearAll()
    }

    fun dropCacheAsync() = viewModelScope.launch(Dispatchers.IO) {
        apolloClient.apolloStore.clearAll()
    }

    data class Task(
        val ministrant: MinistrantFragment,
        val task: TaskFragment,
        val messeId: Int?
    ) {
        val localDateTime by lazy {
            task.start.toLocalDate()
        }

        val calendarEvent by lazy {
            CalendarEvent(task, localDateTime)
        }
    }

    fun fetchTasksAsync(
        after: LocalDateTime = getWeekStart()
    ) = viewModelScope.async(Dispatchers.IO) {
        val networkData = taskRepository.fetchTasks(after)
        if (networkData != null) {
            val tasks = networkData.UserMinistrant.also {
                selectedMinistrants.postValue(it.size)
            }.flatMap { userMinistrant ->
                val ministrant = userMinistrant.ministrantRel.ministrantFragment
                userMinistrant.ministrantRel.tasksRel.map { task ->
                    Task(ministrant, task.taskFragment, task.taskFragment.messeRel?.id)
                }
            }.sortedBy { it.localDateTime }

            if (taskList != tasks) {
                CalendarSyncWorker.enqueue(app, automaticallyAddTasksToCalendar.value, taskReminderTime.value)
            }

            taskList.postValue(tasks)
        }
    }

    fun fetchMesseAsync(id: Int?) = viewModelScope.async(Dispatchers.IO) {
        val data = try {
            fetchMesse(id)
        } catch (e: ApolloException) {
            e.printStackTrace()
            null
        }

        data?.Messe?.firstOrNull()
    }

    fun fetchChangelogAsync(
        versionCode: Int = BuildConfig.VERSION_CODE,
        locale: String = Locale.getDefault().language
    ): Deferred<String?> = viewModelScope.async(Dispatchers.IO) {
        apolloClient.query(GetChangelogQuery(Optional.presentIfNotNull(versionCode)))
            .fetchPolicy(FetchPolicy.NetworkOnly)
            .execute().data?.also { Log.d("MainActivity", it.toString()) }?.Changelog?.firstOrNull()
            ?.let {
                if (locale == "de") it.changelog_de else it.changelog_en
            }
    }

    @Throws(ApolloException::class)
    suspend fun fetchMesse(id: Int?) = withContext(Dispatchers.IO) {
        apolloClient.query(GetMesseByIdQuery(Optional.presentIfNotNull(id))).execute().data
    }
}