package fp.miniplan.app.module.calendar.sync

import android.Manifest
import android.accounts.Account
import android.content.AbstractThreadedSyncAdapter
import android.content.ContentProviderClient
import android.content.ContentProviderOperation
import android.content.Context
import android.content.SyncResult
import android.net.Uri
import android.os.Bundle
import android.provider.BaseColumns
import android.provider.CalendarContract
import android.provider.CalendarContract.Calendars
import android.provider.CalendarContract.Reminders
import android.util.Log
import com.apollographql.apollo3.cache.normalized.FetchPolicy
import fp.miniplan.app.R
import fp.miniplan.app.activity.main.task.TaskRepository
import fp.miniplan.app.ext.hasPermission
import fp.miniplan.app.ext.toLocalDate
import fp.miniplan.app.module.calendar.CalendarEvent
import kotlinx.coroutines.runBlocking
import org.koin.core.component.KoinComponent
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module


val calendarSyncAdapterModule = module {
    singleOf(::CalendarSyncAdapter)
}

class CalendarSyncAdapter(
    context: Context,
    private val taskRepository: TaskRepository
) : AbstractThreadedSyncAdapter(context, true), KoinComponent {

    override fun onPerformSync(
        account: Account,
        extras: Bundle,
        authority: String,
        contentProviderClient: ContentProviderClient,
        syncResult: SyncResult
    ) {
        performSync(
            extras.getBoolean(CalendarSyncAdapterManager.addToCalendarId),
            extras.getInt(CalendarSyncAdapterManager.reminderMinutesId)
        )
    }

    fun performSync(addToCalendar: Boolean, reminderMinutes: Int) {
        if (!context.hasPermission(Manifest.permission.READ_CALENDAR)
            || !context.hasPermission(Manifest.permission.WRITE_CALENDAR)) {
            return
        }

        Log.d("CalendarSyncAdapter", "Performing sync..")
        Log.d("CalendarSyncAdapter", "Getting calendar..")
        val calendarId = getOrCreateCalendar(context)
        Log.d("CalendarSyncAdapter", "Calendar id is $calendarId")

        if (calendarId == -1L) {
            Log.e("CalendarSyncAdapter", "Unable to create calendar")
            return
        }

        Log.d("CalendarSyncAdapter", "Deleting calendar events")
        deleteCalendarEvents(calendarId)

        if (addToCalendar) {
            Log.d("CalendarSyncAdapter", "Adding to calendar")

            val uri = buildUri(CalendarContract.Events.CONTENT_URI)
            val tasks = runBlocking {
                taskRepository.fetchTasks(fetchPolicy = FetchPolicy.CacheOnly)
            }

            if (tasks != null) {
                var selectedMinistrants: Int
                val events = tasks.UserMinistrant.also {
                    selectedMinistrants = it.size
                    Log.d("CalendarSyncAdapter", "Selected $selectedMinistrants ministranten")
                }.flatMap { u ->
                    Log.d("CalendarSyncAdapter", "Mapping $u")

                    val ministrant = u.ministrantRel.ministrantFragment
                    u.ministrantRel.tasksRel.also {
                        Log.d("CalendarSyncAdapter", "Have ${it.size} tasks")
                    }.map { task ->
                        val localDateTime = task.taskFragment.start.toLocalDate()
                        CalendarEvent(task.taskFragment, localDateTime) to ministrant
                    }
                }

                val calendarEvents = events.map { (event, ministrant) ->
                    Log.d("CalendarSyncAdapter", "Built event $event")

                    event.buildEvent(
                        uri,
                        calendarId,
                        if (selectedMinistrants > 1) ministrant.firstname else null
                    )
                }

                Log.d("CalendarSyncAdapter", "Creating ${calendarEvents.size} new events..")

                val reminders = if (reminderMinutes != -1) {
                    List(calendarEvents.size) { index ->
                        ContentProviderOperation.newInsert(buildUri(Reminders.CONTENT_URI)).apply {
                            withValueBackReference(Reminders.EVENT_ID, index)
                            withValue(Reminders.MINUTES, reminderMinutes)
                            withValue(Reminders.METHOD, Reminders.METHOD_ALERT)
                        }.build()
                    }
                } else emptyList()

                context.contentResolver.applyBatch(
                    CalendarContract.AUTHORITY,
                    ArrayList(calendarEvents + reminders)
                )
            }
        }
    }

    private fun deleteCalendarEvents(calendarId: Long) {
        val deletedEvents: Int = context.contentResolver.delete(
            buildUri(CalendarContract.Events.CONTENT_URI),
            CalendarContract.Events.CALENDAR_ID + " = ?", arrayOf(calendarId.toString())
        )

        Log.d("CalendarSyncAdapter", "Deleted $deletedEvents calendar events")
    }

    private fun buildUri(uri: Uri): Uri {
        return uri.buildUpon()
            .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
            .appendQueryParameter(Calendars.ACCOUNT_NAME, CalendarSyncAdapterManager.accountName)
            .appendQueryParameter(Calendars.ACCOUNT_TYPE, CalendarSyncAdapterManager.accountType)
            .build()
    }

    private fun getOrCreateCalendar(context: Context): Long {
        val contentResolver = context.contentResolver
        val calenderUri = buildUri(Calendars.CONTENT_URI)

        val cursor = contentResolver.query(
            calenderUri,
            arrayOf(BaseColumns._ID),
            "${Calendars.ACCOUNT_NAME} = ? AND ${Calendars.ACCOUNT_TYPE} = ?",
            arrayOf(CalendarSyncAdapterManager.accountName, CalendarSyncAdapterManager.accountType),
            null
        )

        return cursor.use {
            if (cursor != null && cursor.moveToNext()) {
                cursor.getLong(0)
            } else {
                val builder = ContentProviderOperation.newInsert(calenderUri).apply {
                    withValue(Calendars.ACCOUNT_NAME, CalendarSyncAdapterManager.accountName)
                    withValue(Calendars.ACCOUNT_TYPE, CalendarSyncAdapterManager.accountType)
                    withValue(Calendars.NAME, "miniplan")
                    withValue(Calendars.CALENDAR_DISPLAY_NAME, context.getString(R.string.calendar_name))
                    withValue(Calendars.CALENDAR_COLOR, 0xA21CAF)
                    withValue(Calendars.CALENDAR_ACCESS_LEVEL, Calendars.CAL_ACCESS_READ)
                    withValue(Calendars.OWNER_ACCOUNT, CalendarSyncAdapterManager.accountName)
                    withValue(Calendars.SYNC_EVENTS, 1)
                    withValue(Calendars.VISIBLE, 1)
                }

                try {
                    contentResolver.applyBatch(
                        CalendarContract.AUTHORITY,
                        arrayListOf(builder.build())
                    )
                } catch (e: Exception) {
                    Log.d("CalendarSyncAdapter", "Exception thrown: ${e.message}")
                    e.printStackTrace()
                    return -1
                }

                getOrCreateCalendar(context)
            }
        }
    }
}
