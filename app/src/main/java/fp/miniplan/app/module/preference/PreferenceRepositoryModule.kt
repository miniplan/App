package fp.miniplan.app.module.preference

import fe.android.preference.helper.PreferenceRepository
import org.koin.dsl.module

val preferenceRepositoryModule = module {
    single { PreferenceRepository(get(), null) }
}