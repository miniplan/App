package fp.miniplan.app.module.service

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import org.koin.dsl.module
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.POST
import java.util.*

val authServiceModule = module {
    single {
        get<Retrofit>().create(AuthService::class.java)
    }
}

interface AuthService {
    @Keep
    data class SmsCode(@SerializedName("phoneNumber") val phoneNumber: String)

    @POST("login")
    fun requestSmsCode(@Body code: SmsCode): Call<String>

    @Keep
    data class Signup(
        @SerializedName("token") val token: String,
        @SerializedName("firstname") val firstname: String,
        @SerializedName("lastname") val lastname: String,
        @SerializedName("phoneNumber") val phoneNumber: String
    )

    @POST("signup")
    fun signup(@Body signup: Signup): Call<String>

    @Keep
    data class VerifySmsCodeRequest(
        @SerializedName("phoneNumber") val phoneNumber: String,
        @SerializedName("otp") val otp: String
    )

    @Keep
    data class JwtResponse(
        @SerializedName("accessToken") val accessToken: String,
        @SerializedName("refreshToken") val refreshToken: String,
        @SerializedName("userInfo") val userInfo: UserInfo
    )

    @Keep
    data class AuthTokenResponse(
        @SerializedName("authToken") val authToken: String,
        @SerializedName("userInfo") val userInfo: UserInfo
    )

    @Keep
    data class UserInfo(
        @SerializedName("firstname") val firstname: String,
        @SerializedName("lastname") val lastname: String,
        @SerializedName("phoneNumber") val phoneNumber: String,
        @SerializedName("userId") val userId: UUID,
    )

    @POST("v2/login/confirm")
    fun verifyCodeV2(@Body verifyRequest: VerifySmsCodeRequest): Call<AuthTokenResponse>

    @POST("login/confirm")
    fun verifyCode(@Body verifyRequest: VerifySmsCodeRequest): Call<JwtResponse>
}