package fp.miniplan.app.module.viewmodel.base

import fp.miniplan.app.module.viewmodel.*
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module

val viewModelModule = module {
    viewModelOf(::SetupViewModel)
    viewModelOf(::SettingsViewModel)
    viewModelOf(::MainViewModel)
    viewModelOf(::LoginLoadViewModel)
    viewModelOf(::LoginViewModel)
    viewModelOf(::MesseViewModel)
}
