package fp.miniplan.app.module.calendar.sync

import android.app.Service
import android.content.Intent
import android.os.IBinder
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class CalendarSyncAdapterService : Service(), KoinComponent {
    private val syncAdapter by inject<CalendarSyncAdapter>()

    override fun onBind(intent: Intent): IBinder? {
        return syncAdapter.syncAdapterBinder
    }
}
