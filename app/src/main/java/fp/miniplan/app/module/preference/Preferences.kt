package fp.miniplan.app.module.preference

import androidx.compose.runtime.*
import fe.android.preference.helper.BasePreference
import fe.android.preference.helper.BasePreference.MappedPreference.Companion.mappedPreference
import fe.android.preference.helper.BasePreference.Preference.Companion.booleanPreference
import fe.android.preference.helper.BasePreference.Preference.Companion.intPreference
import fe.android.preference.helper.BasePreference.PreferenceNullable.Companion.stringPreference
import fp.miniplan.app.util.SetupState

//val preferencesModule = module {
//    single {
//        Preferences(get())
//    }
//}
//
//typealias IntPersister<T> = (T) -> Int
//typealias IntReader<T> = (Int) -> T?
//
//class PreferenceObservable<T>(
//    initial: T,
//    writeHook: (T) -> Unit
//) {
//    var current = initial
//        private set
//    private val observers = mutableListOf(writeHook)
//
//    fun updateValue(newValue: T) {
//        current = newValue
//        observers.forEach {
//            it(newValue)
//        }
//    }
//
//    @Composable
//    fun observeAsState(): State<T> {
//        val lifecycleOwner = LocalLifecycleOwner.current
//        val state = remember { mutableStateOf(current) }
//        DisposableEffect(this, lifecycleOwner) {
//            val observer: (T) -> Unit = {
//                state.value = it
//            }
//
//            observers.add(observer)
//            onDispose { observers.remove(observer) }
//        }
//
//        return state
//    }
//}

object Preferences {
    val jwtAccessToken = stringPreference("jwt_access_token")
    val jwtRefreshToken = stringPreference("jwt_refresh_token")
    val userId = stringPreference("user_id")
    val fullName = stringPreference("full_name")
    val phoneNumber = stringPreference("phone_number")
    val inviteToken = stringPreference("invite_token")
    val tokenUsed = booleanPreference("token_used")
    val notifyOnNewTasksAvailable = booleanPreference("notify_new_tasks", true)
    val automaticallyAddTasksToCalendar = booleanPreference("automatically_add_tasks_to_calendar")
    val taskReminderTime = intPreference("task_reminder_time", -1)
    val lastLaunchedVersion = intPreference("last_launched_version", 0)
    val crashLogsEnabled = booleanPreference("acra.enable", true)
    val authToken = stringPreference("auth_token")

    val setupState = mappedPreference(
        "setup_state",
        SetupState.Login,
        SetupState.Companion
    )

    val all = listOf(
        jwtAccessToken,
        jwtRefreshToken,
        userId,
        fullName,
        phoneNumber,
        inviteToken,
        tokenUsed,
        setupState,
        notifyOnNewTasksAvailable,
        automaticallyAddTasksToCalendar,
        taskReminderTime,
        lastLaunchedVersion,
        crashLogsEnabled,
        authToken
    )

}
