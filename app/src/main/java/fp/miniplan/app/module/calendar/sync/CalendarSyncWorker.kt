package fp.miniplan.app.module.calendar.sync

import android.app.Application
import android.content.Context
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.util.concurrent.TimeUnit

class CalendarSyncWorker(
    appContext: Context,
    workerParams: WorkerParameters
) : Worker(appContext, workerParams), KoinComponent {

    private val calendarSyncAdapter by inject<CalendarSyncAdapter>()

    override fun doWork(): Result {
        calendarSyncAdapter.performSync(
            inputData.getBoolean(CalendarSyncAdapterManager.addToCalendarId, false),
            inputData.getInt(CalendarSyncAdapterManager.reminderMinutesId, -1)
        )

        return Result.success()
    }

    companion object {
        fun enqueue(context: Context, addToCalendar: Boolean, taskReminder: Int) {
            WorkManager.getInstance(context).enqueue(build(addToCalendar, taskReminder))
        }

        fun build(
            addToCalendar: Boolean,
            taskReminder: Int
        ) = OneTimeWorkRequestBuilder<CalendarSyncWorker>().setInitialDelay(
            0,
            TimeUnit.SECONDS
        ).setInputData(
            workDataOf(
                CalendarSyncAdapterManager.addToCalendarId to addToCalendar,
                CalendarSyncAdapterManager.reminderMinutesId to taskReminder
            )
        ).build()
    }
}