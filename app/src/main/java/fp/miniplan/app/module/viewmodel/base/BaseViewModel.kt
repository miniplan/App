package fp.miniplan.app.module.viewmodel.base

import androidx.lifecycle.ViewModel
import fe.android.preference.helper.BasePreference
import fe.android.preference.helper.PreferenceRepository
import fe.android.preference.helper.compose.RepositoryState

abstract class BaseViewModel(preferenceRepository: PreferenceRepository? = null) : ViewModel() {
}