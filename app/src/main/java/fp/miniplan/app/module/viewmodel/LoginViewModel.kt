package fp.miniplan.app.module.viewmodel

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.viewModelScope
import fe.android.preference.helper.PreferenceRepository
import fe.android.preference.helper.compose.getBooleanState
import fe.android.preference.helper.compose.getState
import fe.android.preference.helper.compose.getStringState
import fp.miniplan.app.ext.OnError
import fp.miniplan.app.ext.RequestException
import fp.miniplan.app.ext.executeHandleErrors
import fp.miniplan.app.module.preference.Preferences
import fp.miniplan.app.module.service.AuthService
import fp.miniplan.app.module.token.AccessTokenProvider
import fp.miniplan.app.module.viewmodel.base.BaseViewModel
import fp.miniplan.app.util.phone.isValidPhoneNumber
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

class LoginViewModel(
    val preferenceRepository: PreferenceRepository,
    val accessTokenProvider: AccessTokenProvider,
    private val authService: AuthService
) : BaseViewModel(preferenceRepository) {
    val inviteToken = preferenceRepository.getStringState(Preferences.inviteToken)
    val setupState = preferenceRepository.getState(Preferences.setupState)

    private val serverConnectFailedError = "server_connect_failed"
    private val serverError = "server_error"

    var loginStage by mutableStateOf(LoginStage.EnterPhone)
        private set

    var phoneNumber = MutableStateFlow("")

    var firstname by mutableStateOf("")
    var lastname by mutableStateOf("")
    var signupToken by mutableStateOf("")

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    var validPhoneNumber = phoneNumber.debounce(1000).distinctUntilChanged().flatMapLatest {
        flowOf(isValidPhoneNumber(it, "AT"))
    }

    var code by mutableStateOf("")
    var retrySmsSendInSeconds by mutableIntStateOf(60)

    var loginError by mutableStateOf<String?>(null)
    private val loginErrorPair: Pair<OnError, String> = Pair(
        {
            Log.d("LoginViewModel", "LoginError: $it")
            loginError = if (it is RequestException) {
                it.message
            } else serverConnectFailedError
        }, serverError
    )


    fun changeStage(loginStage: LoginStage) {
        this.loginStage = loginStage
        this.loginError = null
    }

    suspend fun requestSmsCode(): String? {
        this.loginError = null

        return withContext(Dispatchers.IO) {
            authService
                .requestSmsCode(AuthService.SmsCode(phoneNumber.value))
                .executeHandleErrors(loginErrorPair)
                ?.also {
                    retrySmsSendInSeconds = 60
                }
        }
    }

    fun requestSmsCodeAsync() {
        viewModelScope.launch {
            val response = requestSmsCode()
            if (response != null) {
                changeStage(LoginStage.EnterCode)
            }
        }
    }

    fun verifyCodeAsync(): Deferred<AuthService.JwtResponse?> {
        this.loginError = null

        return viewModelScope.async(Dispatchers.IO) {
            authService
                .verifyCode(AuthService.VerifySmsCodeRequest(phoneNumber.value, code))
                .executeHandleErrors(loginErrorPair)
        }
    }

    enum class LoginStage {
        EnterPhone, EnterCode
    }
}


