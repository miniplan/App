package fp.miniplan.app.activity.main.messe

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Optional
import com.apollographql.apollo3.cache.normalized.FetchPolicy
import com.apollographql.apollo3.cache.normalized.emitCacheMisses
import com.apollographql.apollo3.cache.normalized.fetchPolicy
import com.apollographql.apollo3.exception.ApolloException
import fp.miniplan.GetMessenAfterQuery
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.joda.time.LocalDateTime
import org.koin.dsl.module

val messeRepository = module {
    single {
        MesseRepository(get())
    }
}

class MesseRepository(val apollo: ApolloClient) {
    @Throws(ApolloException::class)
    suspend fun fetchMessen(
        after: LocalDateTime = LocalDateTime.now(),
        fetchPolicy: FetchPolicy = FetchPolicy.NetworkFirst
    ) = withContext(Dispatchers.IO) {
        apollo.query(GetMessenAfterQuery(Optional.presentIfNotNull(after.toString())))
            .fetchPolicy(fetchPolicy)
            .emitCacheMisses(false)
            .execute().data
    }
}