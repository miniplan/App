package fp.miniplan.app.activity.main.task

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.android.material.card.MaterialCardView
import fp.miniplan.app.R
import fp.miniplan.app.module.viewmodel.MainViewModel
import fp.miniplan.app.ext.hasPermission
import fp.miniplan.app.util.TaskType
import fp.miniplan.app.util.dateFormatter
import fp.miniplan.app.util.timeFormatter

class TaskViewAdapter internal constructor(
    private val context: Context
) : RecyclerView.Adapter<TaskViewAdapter.ViewHolder>() {

    var multipleMinistranten: Boolean = false
        private set

    private var data: List<MainViewModel.Task> = emptyList()

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var btnClickListener: ItemClickListener? = null
    private var cardClickListener: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.task_row, parent, false))
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = data[position]

        val tvTimeStr = buildString {
            if (multipleMinistranten) {
                this.append("${task.ministrant.firstname}: ")
            }

            this.append(timeFormatter.print(task.localDateTime))

            task.task.description?.let { desc ->
                if (desc.isNotEmpty()) {
                    this.append(" - ").append(desc)
                }
            }
        }

        val tvDescriptionStr = buildString {
            this.append(task.task.taskTypeRel?.name)
            this.append(" - ").append(task.task.messeRel?.messeTypeRel?.name)

            task.task.messeRel?.comment?.let { comment ->
                if (comment.isNotEmpty()) {
                    this.append(" (").append(comment).append(")")
                }
            }

            task.task.description?.let { desc ->
                if (desc.isNotEmpty()) {
                    this.append(" (").append(desc).append(")")
                }
            }
        }

        holder.tvTime.text = tvTimeStr
        holder.tvDate.text = dateFormatter.print(task.localDateTime)
        holder.tvDescription.text = tvDescriptionStr
        holder.ivIcon.setImageResource(TaskType.safeValueOf(task.task.taskTypeRel?.short_name).icon)
        updateCalendarButtonText(task, holder.btAddCalendar)
    }

    fun updateAllButtons() {

    }

    fun updateCalendarButtonText(task: MainViewModel.Task, button: Button) {
        getState(context, task).apply(context, button as MaterialButton)
    }

    private fun getState(context: Context, task: MainViewModel.Task): ButtonState {
        if (!context.hasPermission(Manifest.permission.READ_CALENDAR)) {
            return ButtonState.PermissionMissing
        }

        return with(task.calendarEvent.getCalendarCursor(context.contentResolver)) {
            if (this.count.also { this.close() } >= 1) ButtonState.View else ButtonState.Add
        }
    }

    enum class ButtonState(val string: Int, private val icon: Int, private val textColor: Int) {
        Add(R.string.calendar_add, R.drawable.ic_add_24dp, R.attr.colorPrimary),
        View(R.string.calendar_view, R.drawable.ic_preview_24dp, R.attr.colorPrimary),
        PermissionMissing(
            R.string.grant_calendar_permission,
            R.drawable.ic_error_outline_black_18dp, R.attr.colorError
        );

        fun apply(context: Context, button: MaterialButton) {
            button.text = context.getString(string)
            button.setIconResource(icon)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        var cardView: MaterialCardView = itemView.findViewById(R.id.task_card)
        var tvTime: TextView = itemView.findViewById(R.id.tv_time)
        var tvDate: TextView = itemView.findViewById(R.id.tv_item_date)
        var tvDescription: TextView = itemView.findViewById(R.id.tv_item_description)
        val ivIcon: ImageView = itemView.findViewById(R.id.iv_item_icon)
        val btAddCalendar: Button = itemView.findViewById(R.id.bt_add_calendar)

        init {
            btAddCalendar.setOnClickListener(this)
            cardView.setOnClickListener {
                cardClickListener?.invoke(bindingAdapterPosition)
            }
        }

        override fun onClick(view: View) {
            btnClickListener?.onItemClick(view, bindingAdapterPosition)
        }
    }

    fun getItem(id: Int): MainViewModel.Task {
        return data[id]
    }

    fun setBtnClickListener(itemClickListener: ItemClickListener) {
        this.btnClickListener = itemClickListener
    }

    fun setCardClickListener(fn: (Int) -> Unit){
        cardClickListener = fn
    }

    fun setItems(it: List<MainViewModel.Task>) {
        this.data = it
        this.notifyDataSetChanged()
    }

    fun hasMultipleMinistranten(multipleMinistranten: Boolean) {
        this.multipleMinistranten = multipleMinistranten
    }

    interface ItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}