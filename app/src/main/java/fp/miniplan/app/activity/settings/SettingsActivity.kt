package fp.miniplan.app.activity.settings

import android.Manifest
import android.database.DatabaseUtils
import android.os.Bundle
import android.provider.CalendarContract
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.work.WorkManager
import fp.miniplan.app.BuildConfig
import fp.miniplan.app.R
import fp.miniplan.app.activity.main.MainActivity
import fp.miniplan.app.ext.changeActivity
import fp.miniplan.app.module.calendar.sync.CalendarSyncWorker
import fp.miniplan.app.module.viewmodel.SettingsViewModel
import fp.miniplan.app.ui.composable.BackTopBar
import fp.miniplan.app.ui.composable.BooleanPreference
import fp.miniplan.app.ui.composable.ClickableColumn
import fp.miniplan.app.ui.theme.ComposeMaterialYou
import fp.miniplan.app.ui.theme.HkGroteskFontFamily
import fp.miniplan.app.util.PermissionHelper
import fp.miniplan.app.util.phone.formatPhoneNumber
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinComponent

class SettingsActivity : ComponentActivity(), KoinComponent {
    private val settingsViewModel by viewModel<SettingsViewModel>()
    private val permissionHelper = PermissionHelper(this@SettingsActivity, this@SettingsActivity)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ComposeMaterialYou {
                Scaffold(topBar = {
                    BackTopBar(
                        stringResource(id = R.string.settings), onBackPressed = {
                            changeActivity(MainActivity::class, finish = true)

//                            this.finish()
                        }
                    )
                }) {
                    SettingsScreen(it)
                }
            }
        }
    }

    @Composable
    fun SettingsScreen(paddingValues: PaddingValues) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 5.dp + paddingValues.calculateTopPadding())
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
//                    .clickable {
//
//                    }
                    .padding(horizontal = 15.dp, vertical = 5.dp)
            ) {
                Image(
                    imageVector = Icons.Filled.AccountCircle,
                    modifier = Modifier.size(40.dp),
                    colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.tertiary),
                    contentDescription = "user icon"
                )
                Spacer(modifier = Modifier.width(7.dp))
                Column {
                    Text(
                        text = settingsViewModel.fullName.value!!,
                        fontFamily = HkGroteskFontFamily,
                        fontWeight = FontWeight.Bold,
                        fontSize = 20.sp
                    )

                    Text(
                        text = formatPhoneNumber(settingsViewModel.phoneNumber.value!!)
                            ?: settingsViewModel.phoneNumber.value!!,
                        color = MaterialTheme.colorScheme.tertiary
                    )
                }
            }


//            OutlinedButton(modifier = Modifier.padding(horizontal = 15.dp), onClick = {
//
//            }) {
//                IconTextButtonContent(text = R.string.change_phone_number, iconVector = Icons.Filled.Edit)
////                Icon(imageVector = Icons.Filled.Edit, contentDescription = "edit icon")
////                Text(text = stringResource(id = R.string.change_phone_number))
//            }

            Spacer(modifier = Modifier.height(10.dp))
            Text(
                modifier = Modifier.padding(horizontal = 15.dp),
                text = stringResource(id = R.string.settings),
                fontWeight = FontWeight.Medium,
                fontSize = 17.sp
            )

            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 15.dp)
            ) {
                item {
                    BooleanPreference(state = settingsViewModel.notifyOnNewTasksAvailable) {
                        Text(text = stringResource(id = R.string.notify_on_new_tasks))
                    }
                }

                item {
                    Spacer(modifier = Modifier.height(5.dp))
                }

                item {
                    BooleanPreference(state = settingsViewModel.crashLogsEnabled) {
                        Text(text = stringResource(id = R.string.crashlogs_enabled))
                        Text(
                            text = stringResource(id = R.string.crashlogs_enabled_explainer),
                            color = MaterialTheme.colorScheme.secondary,
                            fontSize = 15.sp
                        )
                    }
                }

                item {
                    Spacer(modifier = Modifier.height(5.dp))
                }

                item {
                    Text(
                        text = stringResource(id = R.string.calendar),
                        fontWeight = FontWeight.Medium,
                        modifier = Modifier.padding(
                            horizontal = 5.dp
                        )
                    )

                    BooleanPreference(
                        state = settingsViewModel.automaticallyAddTasksToCalendar,
                        onCheckedChange = {
                            enqueueForceRefresh(it, settingsViewModel.taskReminderTime.value) {
                                settingsViewModel.automaticallyAddTasksToCalendar.updateState(it)
                            }
                        }
                    ) {
                        Text(text = stringResource(id = R.string.automatically_add_to_calendar))
                        Text(
                            text = stringResource(id = R.string.automatically_add_to_calendar_explainer),
                            color = MaterialTheme.colorScheme.secondary,
                            fontSize = 15.sp
                        )
                    }

                    var dropdownExpanded by remember { mutableStateOf(false) }

                    ClickableColumn(onClick = { dropdownExpanded = true }) {
                        Text(text = stringResource(id = R.string.reminder))

                        val items = remember {
                            mapOf(
                                -1 to R.string.reminder_disabled,
                                0 to R.string.minutes_0,
                                5 to R.string.minutes_5,
                                10 to R.string.minutes_10,
                                15 to R.string.minutes_15,
                                20 to R.string.minutes_20,
                                25 to R.string.minutes_25,
                                30 to R.string.minutes_30,
                                45 to R.string.minutes_45,
                                60 to R.string.hours_1,
                                120 to R.string.hours_2,
                                180 to R.string.hours_3,
                                360 to R.string.hours_6,
                                540 to R.string.hours_9,
                                720 to R.string.hours_12,
                                1440 to R.string.days_1,
                                2880 to R.string.days_2,
                                10080 to R.string.weeks_1,
                                20160 to R.string.weeks_2,
                                40320 to R.string.weeks_4,
                            )
                        }

                        Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .wrapContentSize(Alignment.TopStart)

                        ) {
                            Text(
                                stringResource(id = items[settingsViewModel.taskReminderTime.value]!!),
                                color = MaterialTheme.colorScheme.secondary,
                                modifier = Modifier.fillMaxWidth()
                            )

                            DropdownMenu(
                                expanded = dropdownExpanded,
                                onDismissRequest = { dropdownExpanded = false },
                                modifier = Modifier.fillMaxWidth()
                            ) {
                                items.forEach { (key, value) ->
                                    DropdownMenuItem(text = {
                                        Text(text = stringResource(id = value))
                                    }, onClick = {
                                        settingsViewModel.taskReminderTime.updateState(key)
                                        dropdownExpanded = false

                                        enqueueForceRefresh(
                                            settingsViewModel.automaticallyAddTasksToCalendar.value,
                                            key
                                        )
                                    })
                                }
                            }
                        }
                    }

                    Button(onClick = {
                        enqueueForceRefresh(
                            settingsViewModel.automaticallyAddTasksToCalendar.value,
                            settingsViewModel.taskReminderTime.value
                        )
                    }) {
                        Text(text = stringResource(id = R.string.force_sync_calendar))
                    }

                    if (BuildConfig.DEBUG) {
                        Button(onClick = {
                            debugCalendar()
                        }) {
                            Text(text = stringResource(id = R.string.debug_calendar))
                        }

                        Button(onClick = {
                            settingsViewModel.dropCache()
                        }) {
                            Text(text = stringResource(id = R.string.drop_cache))
                        }

                        Button(onClick = {
                            settingsViewModel.debugCache()
                        }) {
                            Text(text = stringResource(id = R.string.drop_cache))
                        }
                    }
                }
            }
        }
    }

    private fun enqueueForceRefresh(
        addToCalendar: Boolean,
        taskReminder: Int,
        hasPermission: () -> Unit = {}
    ) {
        permissionHelper.runWithPermission(
            arrayOf(
                Manifest.permission.READ_CALENDAR,
                Manifest.permission.WRITE_CALENDAR
            ), null
        ) {
            hasPermission()
            CalendarSyncWorker.enqueue(this, addToCalendar, taskReminder)
        }
    }

    private fun debugCalendar() {
        val allCursor = contentResolver.query(
            CalendarContract.Events.CONTENT_URI, arrayOf(
                CalendarContract.Events.CALENDAR_ID,
                CalendarContract.Events.DTSTART,
                CalendarContract.Events.DTEND,
                CalendarContract.Events.TITLE,
                CalendarContract.Events.STATUS,
                CalendarContract.Events.HAS_ALARM
            ), null, null, null
        )

        allCursor?.use {
            while (it.moveToNext()) {
                Log.d("CalendarDebug", DatabaseUtils.dumpCursorToString(it))
            }
        }
    }
}