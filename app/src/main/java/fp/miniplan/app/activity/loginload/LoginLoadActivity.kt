package fp.miniplan.app.activity.loginload

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.os.bundleOf
import fp.miniplan.app.BuildConfig
import fp.miniplan.app.R
import fp.miniplan.app.activity.main.MainActivity
import fp.miniplan.app.ext.changeAndSaveActivity
import fp.miniplan.app.module.viewmodel.LoginLoadViewModel
import fp.miniplan.app.ui.theme.ComposeMaterialYou
import fp.miniplan.app.ui.theme.HkGroteskFontFamily
import fp.miniplan.app.util.SetupState
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinComponent
import org.koin.core.component.get

class LoginLoadActivity : ComponentActivity(), KoinComponent {
    private val loginLoadViewModel  by viewModel<LoginLoadViewModel>()

    companion object {
        private val signInUrl = Regex(BuildConfig.SIGNIN_REGEX)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        intent?.data?.let {
            signInUrl.matchEntire(it.toString())?.groupValues?.let { list ->
                val (_, token) = list

                loginLoadViewModel.getJWTAsync(token).invokeOnCompletion {
                    changeAndSaveActivity(
                        get(),
                        SetupState.Done,
                        bundle = bundleOf(MainActivity.openMinistrantSelectorSheet to true)
                    )
                }
            }
        }

        setContent {
            ComposeMaterialYou {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Column(
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier.fillMaxWidth().padding(horizontal = 10.dp)
                    ) {
                        Text(
                            text = stringResource(id = R.string.loading_login),
                            fontFamily = HkGroteskFontFamily,
                            fontWeight = FontWeight.Bold,
                            fontSize = 20.sp,
                        )

                        Spacer(modifier = Modifier.height(5.dp))

                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            CircularProgressIndicator()
                        }
                    }
                }
            }
        }
    }
}