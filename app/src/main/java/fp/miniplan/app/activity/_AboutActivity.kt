package fp.miniplan.app.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.marcoscg.licenser.Library
import com.marcoscg.licenser.License
import com.marcoscg.licenser.LicenserDialog
import fp.miniplan.app.BuildConfig
import fp.miniplan.app.R
import fp.miniplan.app.databinding.ActivityAboutBinding
import fp.miniplan.app.util.TextViewUrl

class _AboutActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAboutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAboutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setSupportActionBar(binding.toolbar)

        supportActionBar?.apply {
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }

        TextViewUrl.createClickable(
            this, R.string.website, binding.tvMiniplanWeb, mapOf(
                "web_link" to "https://miniapp.at"
            ), R.color.md_theme_light_primary
        )

        binding.tvAppVersion.text = BuildConfig.VERSION_NAME

        binding.btLicenses.setOnClickListener {

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}