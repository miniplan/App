package fp.miniplan.app.activity.main.task

import android.Manifest
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import fp.miniplan.app.R
import fp.miniplan.app.activity.MesseBottomSheetFragment
import fp.miniplan.app.activity.MiniSelectorBottomSheetFragment
import fp.miniplan.app.module.viewmodel.MainViewModel
import fp.miniplan.app.databinding.FragmentTaskBinding
import fp.miniplan.app.util.PermissionHelper
import fp.miniplan.app.util.VisibilityCondition
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent

class TaskFragment(
    private val viewModel: MainViewModel?
) : Fragment(), KoinComponent, TaskViewAdapter.ItemClickListener {
    constructor() : this(null)

    private lateinit var binding: FragmentTaskBinding
    private var initialOpenJob: Deferred<Unit>? = null

    private lateinit var adapter: TaskViewAdapter

    private lateinit var permissionHelper: PermissionHelper
    private var lastPosition: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        this.binding = FragmentTaskBinding.inflate(inflater, container, false)
        this.permissionHelper = PermissionHelper(this, requireContext())

        this.adapter = TaskViewAdapter(this.requireContext())
        this.adapter.setBtnClickListener(this)

        binding.rvTasks.layoutManager = LinearLayoutManager(this.requireContext())
        binding.rvTasks.adapter = adapter

        val taskVisibilityCondition = VisibilityCondition(
            mutableListOf(binding.tvEmpty, binding.ivEmpty), mutableListOf(binding.rvTasks)
        )

        viewModel?.selectedMinistrants?.observe(viewLifecycleOwner) { ministrantenAmount ->
            this.adapter.hasMultipleMinistranten(ministrantenAmount > 1)
            this.binding.tvEmpty.text = getString(
                if (ministrantenAmount == 0) R.string.no_ministranten_selected
                else R.string.no_tasks_found
            )
        }

        viewModel?.taskList?.observe(viewLifecycleOwner) { tasks ->
            Log.d("Tasklist", "Loaded $tasks")
            taskVisibilityCondition.update(tasks.isEmpty())
            this.adapter.setItems(tasks)
        }

        binding.swipeContainer.apply {
            this.setOnRefreshListener {
                lifecycleScope.launch {
                    refreshTasks()
                }
            }
        }

        binding.fabEdit.setOnClickListener {
            initialOpenJob?.cancel()
            MiniSelectorBottomSheetFragment { refreshTasks() }.show(parentFragmentManager)
        }

        this.adapter.setCardClickListener {
            val item = this.adapter.getItem(it)
            lifecycleScope.launch {
                val messe = viewModel?.fetchMesse(item.messeId)?.Messe?.firstOrNull()
                if (messe != null) {
                    MesseBottomSheetFragment(messe.messeFragment).show(parentFragmentManager)
                }
            }
        }


        return binding.root
    }

    private suspend fun refreshTasks() {
        binding.swipeContainer.isRefreshing = true
        viewModel?.fetchTasksAsync()?.await()
        binding.swipeContainer.isRefreshing = false
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onItemClick(view: View, position: Int) {
        this.permissionHelper.runWithPermission(arrayOf(Manifest.permission.READ_CALENDAR), {
            this.adapter.notifyDataSetChanged()
        }) {
            val item = this.adapter.getItem(position)

            this.lastPosition = position

            val cursor = item.calendarEvent.getCalendarCursor(requireActivity().contentResolver)
            try {
                this.startActivity(
                    if (cursor.count >= 1) {
                        item.calendarEvent.getViewIntent(cursor)
                    } else {
                        item.calendarEvent.getCreateIntent(if (this.adapter.multipleMinistranten) item.ministrant.firstname else null)
                    }
                )
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            }

            this.adapter.notifyItemChanged(position)
        }
    }

}