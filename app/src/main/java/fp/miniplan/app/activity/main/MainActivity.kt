package fp.miniplan.app.activity.main

import android.app.NotificationManager
import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.getSystemService
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.preference.PreferenceManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import fp.miniplan.app.BuildConfig
import fp.miniplan.app.R
import fp.miniplan.app.activity.about.AboutActivity
import fp.miniplan.app.activity.login.LoginActivity
import fp.miniplan.app.activity.main.messe.MesseFragmentFragment
import fp.miniplan.app.activity.main.task.TaskFragment
import fp.miniplan.app.activity.settings.SettingsActivity
import fp.miniplan.app.databinding.ActivityMainBinding
import fp.miniplan.app.ext.changeActivity
import fp.miniplan.app.ext.onNonNullResult
import fp.miniplan.app.job.TaskJobService
import fp.miniplan.app.module.preference.Preferences
import fp.miniplan.app.module.viewmodel.MainViewModel
import fp.miniplan.app.util.NotificationHelper
import fp.miniplan.app.util.SetupState
import io.noties.markwon.Markwon
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class MainActivity : AppCompatActivity(), KoinComponent {
    private lateinit var binding: ActivityMainBinding

    private val viewModel by viewModel<MainViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (viewModel.setupState.value != SetupState.Done) {
            return changeActivity(viewModel.setupState.value ?: SetupState.Login)
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setSupportActionBar(binding.toolbar)

        Log.d("MainActivity", viewModel.lastLaunchedVersion.value.toString())

//        if (viewModel.lastLaunchedVersion.value < BuildConfig.VERSION_CODE) {
//            val markwon = Markwon.create(this)
//            this.viewModel.fetchChangelogAsync(BuildConfig.VERSION_CODE).onNonNullResult {
//                viewModel.lastLaunchedVersion.updateState(BuildConfig.VERSION_CODE)
//
//                Log.d("MainActivity", it)
//                this.runOnUiThread {
//                    MaterialAlertDialogBuilder(this)
//                        .setTitle(resources.getString(R.string.changelog))
//                        .setMessage(markwon.toMarkdown(it))
//                        .setNeutralButton(resources.getString(R.string.dismiss)) { _, _ -> }
//                        .show()
//                }
//            }
//        }

        val bottomNavigationView = binding.bottomNavigationView

        val taskFragment by lazy { TaskFragment(viewModel) }
        val messeFragment by lazy { MesseFragmentFragment() }

        setCurrentFragment(taskFragment)

        bottomNavigationView.setOnItemSelectedListener {
            Log.d("MenuItem", "${it.itemId} ${R.id.taskFragment} ${R.id.messeFragment}")
            when (it.itemId) {
                R.id.page_1 -> {
                    setCurrentFragment(taskFragment)
                }

                R.id.page_2 -> {
                    setCurrentFragment(messeFragment)
                }
            }
            true
        }

        viewModel.inviteToken.updateState(null)

        NotificationHelper.createTaskJobNotificationChannel(this)
        getSystemService<NotificationManager>()?.cancelAll()

        val jobInfo = JobInfo.Builder(
            TASK_FETCH_JOB_ID, ComponentName(this, TaskJobService::class.java)
        ).apply {
            this.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
            this.setPersisted(true)
            this.setPeriodic(RUN_TASK_FETCH, RUN_TASK_FETCH_FLEX)
        }.build()

        val status = getSystemService<JobScheduler>()?.schedule(jobInfo)
        Log.d("job", "scheduled with status $status")

//        intent?.let {
//            Log.d("Intent", "$it")
//            if (it.getBooleanExtra(openMinistrantSelectorSheet, false)) {
//                initialOpenJob = lifecycleScope.async {
//                    delay(1000)
//                    MiniSelectorBottomSheetFragment({}).show(supportFragmentManager)
//                }
//            }
//        }
    }

    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.nav_fragment, fragment)
            commit()
        }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_about -> {
                changeActivity(AboutActivity::class, finish = false)
                true
            }

            R.id.action_settings -> {
                changeActivity(SettingsActivity::class, finish = false)
                true
            }

            R.id.action_logout -> {
                this.viewModel.logoutAsync().invokeOnCompletion {
                    changeActivity(LoginActivity::class, finish = false)
                }

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        const val openMinistrantSelectorSheet = "OPEN_MINISTRANT_SELECTOR_SHEET"

        const val TASK_FETCH_JOB_ID = 123456
        const val RUN_TASK_FETCH: Long = 1000 * 60 * 60 * 24L
        const val RUN_TASK_FETCH_FLEX: Long = 1000 * 60 * 60 * 3L
    }
}