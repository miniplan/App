package fp.miniplan.app.activity.about

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.marcoscg.licenser.Library
import com.marcoscg.licenser.License
import fp.miniplan.app.BuildConfig
import fp.miniplan.app.R
import fp.miniplan.app.activity.main.MainActivity
import fp.miniplan.app.ext.changeActivity
import fp.miniplan.app.ui.composable.BackTopBar
import fp.miniplan.app.ui.composable.CustomDialog
import fp.miniplan.app.ui.composable.LinkText
import fp.miniplan.app.ui.composable.LinkTextData
import fp.miniplan.app.ui.theme.ComposeMaterialYou
import fp.miniplan.app.ui.theme.HkGroteskFontFamily
import org.koin.core.component.KoinComponent

class AboutActivity : ComponentActivity(), KoinComponent {

    private val libraries = listOf(
        Library("Gson", "https://github.com/google/gson", License.APACHE2),
        Library(
            "zxing-android-embedded",
            "https://github.com/journeyapps/zxing-android-embedded",
            License.APACHE2
        ),
        Library("zxing", "https://github.com/zxing/zxing", License.APACHE2),
        Library("Guava", "https://github.com/google/guava", License.APACHE2),
        Library("okhttp3", "https://square.github.io/okhttp/", License.APACHE2),
        Library("jsemver", "https://github.com/zafarkhaja/jsemver", License.MIT),
        Library(
            "eleveationimageview",
            "https://github.com/qhutch/ElevationImageView",
            License.APACHE2
        ),
        Library("Licenser", "https://github.com/marcoscgdev/Licenser", License.MIT),
        Library(
            "joda-time-android",
            "https://github.com/dlew/joda-time-android",
            License.APACHE2
        ),
        Library("sdp", "https://github.com/intuit/sdp", License.MIT)
    )

    val iconMap = mapOf(
        "Candle icon" to "https://de.cleanpng.com/png-p11rnp/",
        "Cross icon" to "https://www.flaticon.com/de/kostenloses-icon/kreuz_2242092",
        "Incense icon" to "https://icon-icons.com/de/symbol/Weihrauch/39074",
        "Communion icon" to "https://www.flaticon.com/free-icon/communion_140095",
        "Microphone icon" to "https://www.flaticon.com/authors/freepik",
        "Unknown task icon" to "https://www.flaticon.com/authors/iconixar"
    )

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ComposeMaterialYou {
                Scaffold(topBar = {
                    BackTopBar(
                        stringResource(id = R.string.action_about), onBackPressed = {
                            changeActivity(MainActivity::class, finish = true)
                        }
                    )
                }) {
                    AboutScreen(it)
                }
            }
        }
    }

    @Composable
    fun AboutScreen(paddingValues: PaddingValues) {
        val uriHandler = LocalUriHandler.current
        var dialog by remember { mutableStateOf(false) }

        CustomDialog(showDialog = dialog, onClose = { dialog = false }) {
            Column(horizontalAlignment = Alignment.Start) {
                Text(
                    text = stringResource(id = R.string.licenses),
                    fontSize = 20.sp,
                    fontWeight = FontWeight.SemiBold
                )
                LazyColumn(content = {
                    item {
                        Text(
                            text = stringResource(id = R.string.libraries),
                            fontSize = 18.sp,
                            color = MaterialTheme.colorScheme.onSurface
                        )

                        Spacer(modifier = Modifier.height(2.dp))
                    }

                    libraries.forEach {
                        item {
                            licenseRow(it.title, it.url!!)
                        }
                    }

                    item {
                        Spacer(modifier = Modifier.height(5.dp))
                    }

                    item {
                        Text(
                            text = stringResource(id = R.string.icons),
                            fontSize = 18.sp,
                            color = MaterialTheme.colorScheme.onSurface
                        )

                        Spacer(modifier = Modifier.height(2.dp))
                    }

                    iconMap.forEach { (name, url) ->
                        item {
                            licenseRow(name, url)
                        }
                    }
                })

                TextButton(onClick = { dialog = false }) {
                    Text(text = stringResource(id = R.string.back))
                }
            }
        }

        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(R.drawable.logo_square_512_512),
                contentDescription = "miniplan icon",
                modifier = Modifier.clip(CircleShape).size(100.dp)
            )

            Spacer(modifier = Modifier.height(5.dp))

            Row {
                Text(
                    text = stringResource(id = R.string.app_name),
                    color = MaterialTheme.colorScheme.onSurface,
                    fontWeight = FontWeight.Medium,
                    fontFamily = HkGroteskFontFamily,
                    fontSize = 20.sp,
                )
            }

            Row {
                Text(
                    text = "Version ${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})",
                    color = MaterialTheme.colorScheme.primary,
                    fontWeight = FontWeight.Medium,
                    fontFamily = HkGroteskFontFamily,
                    fontSize = 18.sp,
                )
            }

            Spacer(modifier = Modifier.height(5.dp))

            Row {
                Text(text = stringResource(id = R.string.made_by))
            }

            Row {
                LinkText(
                    textStyle = TextStyle(fontSize = 16.sp),
                    linkTextData = listOf(
                        LinkTextData(
                            text = stringResource(id = R.string.website),
                            tag = "miniapp",
                            annotation = "miniapp.at",
                            onClick = {
                                uriHandler.openUri("https://miniapp.at")
                            },
                        )
                    )
                )
            }

            Spacer(modifier = Modifier.height(10.dp))

            Row {
                OutlinedButton(onClick = {
                    dialog = true
                }) {
                    Text(text = stringResource(id = R.string.licenses))
                }
            }
        }
    }

    @Composable
    fun licenseRow(name: String, url: String) {
        val uriHandler = LocalUriHandler.current
        Row(modifier = Modifier
            .fillMaxWidth()
            .clickable {
                uriHandler.openUri(url)
            }) {
            Text(text = name)
        }
    }
}