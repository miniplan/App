package fp.miniplan.app.activity.main.task

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import fp.miniplan.app.module.viewmodel.SetupViewModel
import fp.miniplan.app.ui.composable.ColumnWithDragHandle
import fp.miniplan.app.ui.theme.HkGroteskFontFamily
import fp.miniplan.app.util.TaskType
import fp.miniplan.app.util.dateTimeFormatter
import fp.miniplan.fragment.MesseFragment
import org.joda.time.LocalDateTime
import org.koin.androidx.compose.koinViewModel

@Composable
fun MesseBottomSheet(messe: MesseFragment) {
    val dateTime = LocalDateTime.parse(messe.start as String)

    Column(
        modifier = Modifier
            .padding(horizontal = 10.dp)
            .padding(bottom = 40.dp)
    ) {
        ColumnWithDragHandle()

        Spacer(modifier = Modifier.height(7.dp))

        Text(
            text = buildString {
                this.append(messe.messeTypeRel?.name)
                if (messe.comment != null) {
                    this.append(" (").append(messe.comment).append(")")
                }
            },
            fontSize = 18.sp,
            fontWeight = FontWeight.SemiBold,
            color = MaterialTheme.colorScheme.onSurface,
            fontFamily = HkGroteskFontFamily
        )

        Text(
            text = dateTimeFormatter.print(dateTime),
            fontSize = 17.sp,
            color = MaterialTheme.colorScheme.onSurfaceVariant
        )

        Spacer(modifier = Modifier.height(8.dp))

        LazyColumn {
            messe.tasksRel.forEach { task ->
                item {
                    Spacer(modifier = Modifier.height(15.dp))
                }

                item {
                    MesseTaskRow(task)
                }
            }
        }
    }
}

@Composable
fun MesseTaskRow(task: MesseFragment.TasksRel) {
    Row {
        Image(
            painter = painterResource(id = TaskType.safeValueOf(task.taskTypeRel?.short_name).icon),
            modifier = Modifier.size(40.dp),
            contentDescription = "task icon"
        )

        Spacer(modifier = Modifier.width(12.dp))

        Column {
            Text(
                text = "%s %s".format(task.ministrantRel?.firstname, task.ministrantRel?.lastname),
                fontSize = 17.sp,
                color = MaterialTheme.colorScheme.onSurface,
                fontWeight = FontWeight.SemiBold,
                fontFamily = HkGroteskFontFamily
            )
            Text(text = task.taskTypeRel!!.name, color = MaterialTheme.colorScheme.tertiary)
        }
    }
}
