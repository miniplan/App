package fp.miniplan.app.activity.setup

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectVerticalDragGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.WifiOff
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import fp.miniplan.app.R
import fp.miniplan.app.ext.asUUID
import fp.miniplan.app.ext.isMatch
import fp.miniplan.app.module.viewmodel.SetupViewModel
import fp.miniplan.app.ui.composable.ColumnWithDragHandle
import fp.miniplan.app.ui.composable.DragHandle
import fp.miniplan.app.ui.composable.SearchTextField
import fp.miniplan.app.ui.composable.rememberScrollContext
import fp.miniplan.app.ui.theme.HkGroteskFontFamily
import fp.miniplan.app.util.ResultWrapper
import fp.miniplan.app.util.hasInternet
import fp.miniplan.fragment.MinistrantFragment
import org.koin.androidx.compose.koinViewModel

@Composable
fun SetupTaskViewScreen(setupViewModel: SetupViewModel) {
    if (!hasInternet(LocalContext.current)) {
        NoInternet()
        return
    }

    val data = setupViewModel.setupData
    Log.d("SetupData", "$data")
    if (data is ResultWrapper.Result && data.result != null) {
        val result = data.result

        MinistrantSelector(
            result.user,
            result.ministranten,
            setupViewModel.selectedMinistranten.isNotEmpty(),
            onCheckboxClick = { mini, b ->
                val miniId = mini.id.asUUID()
                if (b) setupViewModel.selectedMinistranten.add(miniId) else setupViewModel.selectedMinistranten.remove(
                    miniId
                )
            },
            isInitialChecked = { setupViewModel.selectedMinistranten.contains(it.id.asUUID()) },
            isAtTop = {
                setupViewModel.behavior?.isDraggable = it || setupViewModel.allowDrag
                Log.d("Setup", "behavior = ${setupViewModel.behavior?.isDraggable}")
            }, setAllowDrag = {
                setupViewModel.behavior?.isDraggable = setupViewModel.selectorAtTop || it
                Log.d("Setup", "behavior2 = ${setupViewModel.behavior?.isDraggable}")
            })
    }
}

@Composable
fun NoInternet() {
    ColumnWithDragHandle()

    Column(
        modifier = Modifier
            .fillMaxHeight(0.6f)
            .padding(horizontal = 15.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            imageVector = Icons.Rounded.WifiOff,
            modifier = Modifier.size(60.dp),
            colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.tertiary),
            contentDescription = "wifi off icon"
        )

        Spacer(modifier = Modifier.height(10.dp))

        Text(
            text = stringResource(id = R.string.no_internet),
            fontSize = 18.sp,
            fontWeight = FontWeight.SemiBold,
            color = MaterialTheme.colorScheme.onSurface
        )
    }
}

@Composable
fun MinistrantSelector(
    ministrant: MinistrantFragment?,
    ministranten: List<MinistrantFragment>,
    nextEnabled: Boolean,
    onCheckboxClick: ((MinistrantFragment, Boolean) -> Unit),
    isInitialChecked: ((MinistrantFragment)) -> Boolean,
    isAtTop: (Boolean) -> Unit,
    setAllowDrag: (Boolean) -> Unit,
) {

//    Text(
//        text = if (ministrant == null) stringResource(id = R.string.hi) else stringResource(
//            id = R.string.hi_firstname, ministrant.firstname
//        ),
//        fontSize = 23.sp,
//        fontWeight = FontWeight.Bold,
//        fontFamily = HkGroteskFontFamily
//    )


    Spacer(modifier = Modifier.height(10.dp))

    MinistrantSelectorList(
        ministrant = ministrant,
        ministranten = ministranten,
        nextEnabled = nextEnabled,
        onCheckboxClick = onCheckboxClick,
        isInitialChecked = isInitialChecked,
        isAtTop = isAtTop,
        setAllowDrag = setAllowDrag
    )
}

@Composable
fun MinistrantSelectorList(
    ministrant: MinistrantFragment?, ministranten: List<MinistrantFragment>,
    nextEnabled: Boolean,
    onCheckboxClick: ((MinistrantFragment, Boolean) -> Unit),
    isInitialChecked: ((MinistrantFragment)) -> Boolean,
    isAtTop: (Boolean) -> Unit,
    setAllowDrag: (Boolean) -> Unit,
) {
    val searchText = rememberSaveable { mutableStateOf("") }
    val lazyListState = rememberLazyListState()
    val scrollContext = rememberScrollContext(lazyListState)

    isAtTop(scrollContext.isTop)

    Column(modifier = Modifier.fillMaxSize()) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxWidth()
                .pointerInput(Unit) {
                    detectVerticalDragGestures { _, _ ->
                        setAllowDrag(true)
                    }
                }) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentSize(Alignment.Center)
            ) {
                DragHandle()
            }

            Spacer(modifier = Modifier.height(7.dp))

            Text(
                text = stringResource(id = R.string.view_tasks_of_whom),
                fontSize = 18.sp,
                fontWeight = FontWeight.SemiBold,
                color = MaterialTheme.colorScheme.onSurface,
//                textAlign = TextAlign.Center

                fontFamily = HkGroteskFontFamily
            )

            Text(
                text = stringResource(id = R.string.close_auto_saves),
                fontSize = 14.sp,
                color = MaterialTheme.colorScheme.onSurfaceVariant
            )

            Spacer(modifier = Modifier.height(10.dp))

            SearchTextField(searchText.value, onValueChange = {
                searchText.value = it
            })
        }

        Spacer(modifier = Modifier.height(6.dp))

        LazyColumn(modifier = Modifier.weight(1f), state = lazyListState) {
            if (ministrant != null) {
                item {
                    MinistrantRow(
                        ministrant = ministrant,
                        defaultValue = isInitialChecked(ministrant),
                        onCheckboxClick = onCheckboxClick,
                        formatString = "%s %s (${stringResource(id = R.string.you)})"
                    )

                    Divider()
                }
            }

            items(items = ministranten, key = { it.id }) { item ->
                if (searchText.value.isEmpty() || item.isMatch(searchText.value)) {
                    MinistrantRow(
                        ministrant = item,
                        defaultValue = isInitialChecked(item),
                        onCheckboxClick = onCheckboxClick
                    )
                }
            }
        }
//        Button(onClick = {  }, enabled = nextEnabled) {
//            Text(text = stringResource(id = R.string.next))
//        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MinistrantRow(
    ministrant: MinistrantFragment,
    formatString: String = "%s %s",
    defaultValue: Boolean = false,
    onCheckboxClick: ((MinistrantFragment, Boolean) -> Unit)
) {
    val checked = rememberSaveable { mutableStateOf(defaultValue) }

    Row(verticalAlignment = Alignment.CenterVertically, modifier = Modifier
        .fillMaxWidth()
        .clickable {
            checked.value = !checked.value
            onCheckboxClick(ministrant, checked.value)
        }) {
        Checkbox(
            checked = checked.value,
//                colors = CheckboxDefaults.colors(checkedColor = MaterialTheme.colors.primary),
            onCheckedChange = {
                checked.value = it
                onCheckboxClick(ministrant, it)
            })
        Text(
            text = formatString.format(ministrant.lastname, ministrant.firstname),
            color = MaterialTheme.colorScheme.onSurface
        )
    }
}

