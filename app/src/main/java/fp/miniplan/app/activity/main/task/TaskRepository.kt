package fp.miniplan.app.activity.main.task

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.cache.normalized.FetchPolicy
import com.apollographql.apollo3.cache.normalized.api.NormalizedCache
import com.apollographql.apollo3.cache.normalized.apolloStore
import com.apollographql.apollo3.cache.normalized.emitCacheMisses
import com.apollographql.apollo3.cache.normalized.fetchPolicy
import com.apollographql.apollo3.exception.ApolloException
import fp.miniplan.GetUserMinistrantsTasksQuery
import fp.miniplan.app.ext.getWeekStart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.joda.time.LocalDateTime
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

val taskRepositoryModule = module {
    singleOf(::TaskRepository)
}

class TaskRepository(val apollo: ApolloClient, ) {
    @Throws(ApolloException::class)
    suspend fun fetchTasks(
        after: LocalDateTime = getWeekStart(),
        fetchPolicy: FetchPolicy = FetchPolicy.NetworkFirst
    ) = withContext(Dispatchers.IO) {
        apollo.query(GetUserMinistrantsTasksQuery(after.toString()))
            .emitCacheMisses(false)
            .fetchPolicy(fetchPolicy)
            .execute().data
    }
}