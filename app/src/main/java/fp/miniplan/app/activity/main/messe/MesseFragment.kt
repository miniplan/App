package fp.miniplan.app.activity.main.messe

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.CalendarMonth
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import fp.miniplan.app.R
import fp.miniplan.app.activity.MesseBottomSheetFragment
import fp.miniplan.app.module.viewmodel.MesseViewModel
import fp.miniplan.app.ui.composable.SearchTextField
import fp.miniplan.app.ui.theme.ComposeMaterialYou
import fp.miniplan.app.ui.theme.HkGroteskFontFamily
import fp.miniplan.app.util.dateFormatter
import fp.miniplan.app.util.dayFormatter
import fp.miniplan.app.util.timeFormatter
import fp.miniplan.fragment.MesseFragment
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.koin.androidx.viewmodel.ext.android.viewModel

class MesseFragmentFragment : Fragment() {
    private val messeViewModel by viewModel<MesseViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeView(requireContext()).apply {
        setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
        setContent {
            ComposeMaterialYou {
                MessenList()
            }
        }
    }

    private fun MesseFragment.isMatch(text: String): Boolean {
        val date = LocalDateTime.parse(this.start as String)

        return this.messeTypeRel?.name?.contains(text, ignoreCase = true) == true
                || (dateFormatter.print(date)).contains(text, ignoreCase = true)
                || (timeFormatter.print(date)).contains(text, ignoreCase = true)
                || (dayFormatter.print(date)).contains(text, ignoreCase = true)
                || this.comment?.contains(text, ignoreCase = true) == true
    }

    @Composable
    fun MessenList() {
        var isRefreshing by remember { mutableStateOf(false) }
        var searchText by rememberSaveable { mutableStateOf("") }

        val now = LocalDateTime.now()
        val datePickerDialog = DatePickerDialog(
            LocalContext.current,
            { _: DatePicker, year: Int, month: Int, day: Int ->
                searchText = dateFormatter.print(LocalDate(year, month + 1, day))
            },
            now.year,
            now.monthOfYear,
            now.dayOfMonth
        )

        Column(modifier = Modifier.padding(horizontal = 16.dp)) {
            Row {
                Column(modifier = Modifier.weight(9f)) {
                    SearchTextField(text = searchText, onValueChange = {
                        searchText = it
                    })
                }

                Spacer(modifier = Modifier.width(3.dp))

                Column(modifier = Modifier.weight(1f)) {
                    IconButton(modifier = Modifier.size(55.dp), onClick = {
                        datePickerDialog.show()
                    }) {
                        Image(
                            imageVector = Icons.Rounded.CalendarMonth,
                            contentScale = ContentScale.Inside,
                            modifier = Modifier.size(100.dp),
                            contentDescription = "calendar icon",
                            colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onSurface)
                        )
                    }
                }
            }

            Spacer(modifier = Modifier.height(10.dp))

            SwipeRefresh(
                modifier = Modifier.fillMaxSize(),
                state = rememberSwipeRefreshState(isRefreshing),
                onRefresh = {
                    isRefreshing = true
                    Log.d("FetchMessen", "Fetch started")
                    messeViewModel.fetchMessenAsync()
                    Log.d("FetchMessen", "Fetch stopped")
                    isRefreshing = false
                },
            ) {
                LazyColumn(modifier = Modifier.fillMaxSize()) {
                    items(items = messeViewModel.messen, key = { it.messeFragment.id }) { item ->
                        if (item.messeFragment.isMatch(searchText)) {
                            MesseRow(item.messeFragment)
                            Spacer(modifier = Modifier.height(12.dp))
                        }
                    }
                }
            }
        }
    }


    @Composable
    fun MesseRow(it: MesseFragment) {
        val date = LocalDateTime.parse(it.start as String)
        OutlinedCard(modifier = Modifier.fillMaxWidth()) {
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .background(MaterialTheme.colorScheme.surface)
                    .clickable {
                        MesseBottomSheetFragment(it).show(parentFragmentManager)
                    },
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Column(modifier = Modifier.padding(horizontal = 16.dp, vertical = 12.dp)) {
                    Text(
                        text = timeFormatter.print(date),
                        fontSize = 32.sp,
                        fontFamily = HkGroteskFontFamily,
                        fontWeight = FontWeight.SemiBold,
                        color = MaterialTheme.colorScheme.onSurface
                    )

                    Text(
                        text = dateFormatter.print(date),
                        fontSize = 16.sp,
                        color = MaterialTheme.colorScheme.onSurface
                    )

                    Text(
                        text = buildString {
                            this.append(it.messeTypeRel?.name)
                            if (it.comment != null && it.messeTypeRel?.name != it.comment) {
                                this.append(" (").append(it.comment).append(")")
                            }

                            this.append(" (").append(it.tasksRel_aggregate.aggregate?.count)
                                .append(" ")
                                .append(stringResource(id = R.string.tasks)).append(")")
                        },
                        fontSize = 16.sp,
                        color = MaterialTheme.colorScheme.tertiary
                    )
                }
            }
        }
    }
}