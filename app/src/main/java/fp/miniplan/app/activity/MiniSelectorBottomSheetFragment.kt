package fp.miniplan.app.activity

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.lifecycleScope
import androidx.work.WorkManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import fp.miniplan.app.R
import fp.miniplan.app.activity.setup.SetupTaskViewScreen
import fp.miniplan.app.ext.asUUID
import fp.miniplan.app.module.viewmodel.SetupViewModel
import fp.miniplan.app.ui.theme.ComposeMaterialYou
import fp.miniplan.app.util.ResultWrapper
import fp.miniplan.app.module.calendar.sync.CalendarSyncWorker
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class MiniSelectorBottomSheetFragment(val refreshHook: suspend () -> Unit) : BottomSheetDialogFragment() {

    private val setupViewModel by viewModel<SetupViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                ComposeMaterialYou {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(horizontal = 8.dp)
                    ) {
                        SetupTaskViewScreen(setupViewModel = setupViewModel)
                    }
                }
            }
        }
    }


    fun show(fragmentManager: FragmentManager) {
        show(fragmentManager, tag)
    }

    override fun getTheme(): Int {
        return R.style.CustomBottomSheetDialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        val done = {
            removeFragment()

            super.onDismiss(dialog)
        }

        val data = setupViewModel.setupData
        if (data is ResultWrapper.Result) {
            handleResult(data, done)
        } else done()
    }

    private fun handleResult(
        data: ResultWrapper.Result<SetupViewModel.SetupData>,
        done: () -> Unit
    ) {
        val old = data.result?.selectedMinistranten?.map {
            it.id.asUUID()
        }?.toHashSet() ?: emptySet()

        val new = setupViewModel.selectedMinistranten.map { it }.toHashSet()

        val (added, deleted) = with(new + old) {
            this - old to this - new
        }

        Log.d("MiniSelectorBottomSheetFragment", "Added: $added, deleted: $deleted")

        if (added.isEmpty() && deleted.isEmpty()) {
            done()
            return
        }

        lifecycleScope.launch {
            val result = setupViewModel.updateSelection(added, deleted)
            if ((result.first ?: 0) > 0 || (result.second ?: 0) > 0) {
                WorkManager.getInstance(requireActivity()).enqueue(
                    CalendarSyncWorker.build(
                        setupViewModel.automaticallyAddTasksToCalendar.value,
                        setupViewModel.taskReminderTime.value,
                    )
                )

                refreshHook()
            }

            done()
        }
    }

    private fun removeFragment() {
        this.parentFragmentManager.beginTransaction().remove(this).commit()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return (super.onCreateDialog(savedInstanceState) as BottomSheetDialog).apply {
            setOnShowListener {
                val bottomSheetDialog = it as? BottomSheetDialog
                bottomSheetDialog?.setCancelable(true)

                val bottomSheet = bottomSheetDialog?.findViewById<View>(
                    com.google.android.material.R.id.design_bottom_sheet
                ) as? FrameLayout

                if (bottomSheet != null) {
                    setupViewModel.behavior = BottomSheetBehavior.from(bottomSheet)
                }
            }
        }
    }

//    fun test(state: Int): String {
//        return when (state) {
//            BottomSheetBehavior.STATE_DRAGGING -> "dragging"
//            BottomSheetBehavior.STATE_SETTLING -> "settling"
//            BottomSheetBehavior.STATE_EXPANDED -> "expanded"
//            BottomSheetBehavior.STATE_COLLAPSED -> "collapsed"
//            BottomSheetBehavior.STATE_HIDDEN -> "hidden"
//            BottomSheetBehavior.STATE_HALF_EXPANDED -> "half expanded"
//            else -> ""
//        }
//    }
}
