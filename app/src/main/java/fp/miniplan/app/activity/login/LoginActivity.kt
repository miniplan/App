package fp.miniplan.app.activity.login

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent.KEYCODE_ENTER
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.StringRes
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.relocation.BringIntoViewRequester
import androidx.compose.foundation.relocation.bringIntoViewRequester
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.os.bundleOf
import fp.miniplan.app.BuildConfig
import fp.miniplan.app.R
import fp.miniplan.app.activity.main.MainActivity
import fp.miniplan.app.ext.changeAndSaveActivity
import fp.miniplan.app.ext.onNonNullResult
import fp.miniplan.app.module.viewmodel.LoginViewModel
import fp.miniplan.app.ui.bringIntoView
import fp.miniplan.app.ui.composable.IconTextButtonContent
import fp.miniplan.app.ui.gradientBackground
import fp.miniplan.app.ui.gradientColors
import fp.miniplan.app.ui.theme.ComposeMaterialYou
import fp.miniplan.app.ui.theme.HkGroteskFontFamily
import fp.miniplan.app.util.SetupState
import fp.miniplan.app.util.TextViewUrl
import fp.miniplan.app.util.phone.PhoneNumberVisualTransformation
import kotlinx.coroutines.delay
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class LoginActivity : ComponentActivity() {
    private val loginViewModel by viewModel<LoginViewModel>()

    companion object {
        private val regexes = listOf(
            Regex("miniplan:\\/\\/signup\\/(.*)"),
            Regex("https:\\/\\/(?:.*)miniapp\\.at\\/invite\\/(.*)")
        )


        fun findToken(str: String): String? {
            return regexes.firstNotNullOfOrNull { regex -> regex.matchEntire(str) }
                ?.groupValues?.get(1)
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val token = intent?.data?.let { uri ->
            val token = findToken(uri.toString())
            loginViewModel.inviteToken.updateState(token)
            token

        } ?: loginViewModel.inviteToken.value.also {
            Log.d("Reading token", "$it")
        }

        if (token != null) {
            loginViewModel.signupToken = token
        }

        setContent {
            ComposeMaterialYou {
                Crossfade(targetState = loginViewModel.loginStage, label = "") { stage ->
                    ScrollColumn { bringIntoViewRequester ->
                        RootScreen(
                            stage = stage,
                            bringIntoViewRequester = bringIntoViewRequester
                        )
                    }
                }
            }
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun RootScreen(
        stage: LoginViewModel.LoginStage,
        bringIntoViewRequester: BringIntoViewRequester
    ) {
        when (stage) {
//            LoginViewModel.LoginStage.Signup -> PhoneNumberScreenHeader(
//                bringIntoViewRequester,
//                R.string.please_enter_name
//            ) {
//                EnterName(bringIntoViewRequester,
//                    loginViewModel.firstname,
//                    loginViewModel.lastname,
//                    { loginViewModel.firstname = it },
//                    { loginViewModel.lastname = it },
//                    { loginViewModel.changeStage(LoginViewModel.LoginStage.EnterPhone) })
//            }

            LoginViewModel.LoginStage.EnterPhone -> PhoneNumberScreenHeader(
                bringIntoViewRequester,
                R.string.please_login_phonenumber
            ) {
                EnterPhoneNumber(
                    bringIntoViewRequester,
                    loginViewModel.phoneNumber.collectAsState().value,
                    loginViewModel.validPhoneNumber.collectAsState(initial = false).value,
                    loginViewModel.loginError,
                    { loginViewModel.phoneNumber.value = it },
                    { loginViewModel.requestSmsCodeAsync() }
                )
            }

            LoginViewModel.LoginStage.EnterCode ->
                EnterCode(bringIntoViewRequester,
                    loginViewModel.retrySmsSendInSeconds,
                    { loginViewModel.retrySmsSendInSeconds = it },
                    loginViewModel.loginError,
                    loginViewModel.code,
                    { loginViewModel.code = it },
                    { loginViewModel.changeStage(LoginViewModel.LoginStage.EnterPhone) },
                    {
                        loginViewModel.verifyCodeAsync()
                            .onNonNullResult { (accessToken, refreshToken, userInfo) ->
                                loginViewModel.accessTokenProvider.store(
                                    accessToken,
                                    refreshToken,
                                    userInfo
                                )

                                changeAndSaveActivity(
                                    loginViewModel.setupState,
                                    SetupState.Done,
                                    bundle = bundleOf(
                                        MainActivity.openMinistrantSelectorSheet to true
                                    )
                                )

                            }
                    },
                    { loginViewModel.requestSmsCodeAsync() }
                )
        }
    }

    @OptIn(ExperimentalComposeUiApi::class, ExperimentalFoundationApi::class)
    @Composable
    fun ScrollColumn(
        scrollableState: ScrollState = rememberScrollState(),
        content: @Composable ColumnScope.(BringIntoViewRequester) -> Unit,
    ) {
        val keyboardController = LocalSoftwareKeyboardController.current
        val bringIntoViewRequester = remember { BringIntoViewRequester() }
        val coroutineScope = rememberCoroutineScope()

        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxSize()
                .drawBehind(
                    gradientBackground(
                        MaterialTheme.gradientColors(alpha = 0.1f),
                        81f
                    )
                )
                .padding(horizontal = 35.dp)
                .verticalScroll(scrollableState)
                .padding(top = 70.dp)
        ) {
            content(bringIntoViewRequester)
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun PhoneNumberScreenHeader(
        bringIntoViewRequester: BringIntoViewRequester,
        @StringRes subHeadline: Int,
        content: @Composable ColumnScope.() -> Unit,
    ) {
//        val keyboardController = LocalSoftwareKeyboardController.current
//        val bringIntoViewRequester = remember { BringIntoViewRequester() }
//        val coroutineScope = rememberCoroutineScope()

//        Column(
//            verticalArrangement = Arrangement.Center,
//            modifier = Modifier
//                .fillMaxSize()
//                .verticalScroll(scrollableState)
//                .padding(top = 70.dp)
//        ) {
//        Row(horizontalArrangement = Arrangement.Start) {
        Image(
            painter = painterResource(id = R.drawable.mini),
            modifier = Modifier
                .size(250.dp)
                .fillMaxWidth(),
            contentDescription = "hand holding phone"
        )
//        }


        Column(
            horizontalAlignment = Alignment.Start,
            modifier = Modifier.bringIntoViewRequester(bringIntoViewRequester)
        ) {
            Text(
                text = stringResource(id = R.string.welcome),
                fontSize = 23.sp,
                color = MaterialTheme.colorScheme.onSurface,
                fontWeight = FontWeight.Bold,
                fontFamily = HkGroteskFontFamily,
//                modifier = Modifier.weight(1f)
            )

            Text(
                text = stringResource(id = subHeadline),
                color = MaterialTheme.colorScheme.tertiary,
//                modifier = Modifier.weight(1f)
            )

            Spacer(modifier = Modifier.height(12.dp))

            content(this)
        }
//        }
    }

    @OptIn(
        ExperimentalFoundationApi::class, ExperimentalComposeUiApi::class,
        ExperimentalMaterial3Api::class
    )
    @Composable
    fun EnterPhoneNumber(
        bringIntoViewRequester: BringIntoViewRequester,
        phoneNumber: String,
        validPhoneNumber: Boolean,
        loginError: String?,
        onPhoneChange: (String) -> Unit,
        onNextClick: () -> Unit,
    ) {
        val coroutineScope = rememberCoroutineScope()
        val keyboardController = LocalSoftwareKeyboardController.current

        Column {
            TextField(
                singleLine = true,
                leadingIcon = {
                    Icon(
                        imageVector = Icons.Filled.Phone,
                        contentDescription = "Phone icon",
                        modifier = Modifier.size(20.dp)
                    )
                },
                label = { Text(stringResource(id = R.string.phoneNumber)) },
                isError = loginError != null,
                value = phoneNumber,
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Phone),
                visualTransformation = PhoneNumberVisualTransformation("AT"),
                modifier = Modifier
                    .fillMaxWidth()
                    .onKeyEvent {
                        if (it.nativeKeyEvent.keyCode == KEYCODE_ENTER) {
                            onNextClick()
                        }

                        true
                    }
                    .bringIntoView(coroutineScope, bringIntoViewRequester),
                onValueChange = onPhoneChange,
                keyboardActions = KeyboardActions(onDone = {
                    keyboardController?.hide()
                    if (validPhoneNumber) onNextClick()
                })
            )


            ErrorText(loginError)
        }

        Spacer(modifier = Modifier.height(3.dp))

        val color = MaterialTheme.colorScheme.primary.toArgb()
        AndroidView(factory = { context ->
            android.widget.TextView(context).apply {
                TextViewUrl.createClickable(
                    this@LoginActivity,
                    R.string.privacy_policy,
                    this,
                    mapOf("privacy_link" to BuildConfig.PRIVACY_LINK),
                    color
                )
            }
        })

        Spacer(modifier = Modifier.height(12.dp))

        Row {
            Button(enabled = validPhoneNumber, onClick = {
                onNextClick()
            }) {
                Text(stringResource(id = R.string.login))
            }
        }
    }

    @OptIn(
        ExperimentalFoundationApi::class, ExperimentalComposeUiApi::class,
        ExperimentalMaterial3Api::class
    )
    @Composable
    fun EnterCode(
        bringIntoViewRequester: BringIntoViewRequester,
        retrySmsSendInSeconds: Int,
        updateRetrySmsSendSeconds: (Int) -> Unit,
        loginError: String?,
        code: String,
        onCodeChange: (String) -> Unit,
        onBackClick: () -> Unit,
        verifyCodeRequested: () -> Unit,
        smsCodeRequested: () -> Unit,
    ) {
        val coroutineScope = rememberCoroutineScope()
        val keyboardController = LocalSoftwareKeyboardController.current

        LaunchedEffect(key1 = retrySmsSendInSeconds) {
            if (retrySmsSendInSeconds > 0) {
                delay(1000)
                updateRetrySmsSendSeconds(retrySmsSendInSeconds - 1)
            }
        }

        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Image(
                painter = painterResource(id = R.drawable.arrow),
                modifier = Modifier
                    .size(250.dp)
                    .fillMaxWidth(),
                contentDescription = "hand holding arrow"
            )
        }

        Column(
            horizontalAlignment = Alignment.Start
        ) {
            Text(
                text = stringResource(id = R.string.sms_code_sent),
                color = MaterialTheme.colorScheme.onSurface,
                fontSize = 23.sp,
                fontWeight = FontWeight.Bold,
                fontFamily = HkGroteskFontFamily
            )

            Text(
                text = stringResource(id = R.string.sms_code_sent_sub),
                color = MaterialTheme.colorScheme.tertiary
            )

            Spacer(modifier = Modifier.height(12.dp))

            Column {
                TextField(
                    singleLine = true,
                    leadingIcon = {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_vpn_key),
                            contentDescription = "PIN icon",
                            modifier = Modifier.size(20.dp)
                        )
                    },
                    label = { Text(stringResource(id = R.string.sms_code)) },
                    isError = loginError != null,
                    value = code,
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Number,
                        imeAction = ImeAction.Done
                    ),
                    modifier = Modifier
                        .fillMaxWidth()
                        .onKeyEvent {
                            if (it.nativeKeyEvent.keyCode == KEYCODE_ENTER && code.length == 6) {
                                keyboardController?.hide()
                                verifyCodeRequested()
                            }

                            true
                        }
                        .bringIntoView(coroutineScope, bringIntoViewRequester),
                    onValueChange = {
                        if (it.length <= 6) {
                            onCodeChange(it)
                        }

                        if (it.length == 6) {
                            keyboardController?.hide()
                            verifyCodeRequested()
                        }
                    }
                )

//                OtpComposableFilled(
//                    heightInDP = 50.dp,
//                    widthInDp = 50.dp,
//                    cornerRadius = 8.dp,
//                    passwordToggle = false,
//                    automaticCapture = true,
//                    arrangement = Arrangement.SpaceEvenly,
//                    backgroundDrawable = R.drawable.ic_rectangle_background,
//                    modifier = Modifier.fillMaxWidth()
//                )
//                {
//                    Log.d("LogTag", it.toString())
//                }

                ErrorText(loginError)
            }

            Spacer(modifier = Modifier.height(5.dp))

            TextButton(enabled = retrySmsSendInSeconds == 0, onClick = smsCodeRequested) {
                Text(
                    if (retrySmsSendInSeconds == 0) {
                        stringResource(id = R.string.resend_code)
                    } else stringResource(
                        id = R.string.resend_code_in_n_seconds,
                        retrySmsSendInSeconds
                    )
                )
            }

            Spacer(modifier = Modifier.height(5.dp))

            OutlinedButton(onClick = onBackClick) {
                IconTextButtonContent(R.string.go_back, Icons.Filled.ArrowBack)
            }
        }
    }

    @Composable
    fun ErrorText(loginError: String?) {
        if (loginError != null) {
            val key = resources.getIdentifier(loginError, "string", packageName)

            Text(
                text = if (key != 0) resources.getString(key) else stringResource(id = R.string.server_error),
                color = MaterialTheme.colorScheme.error,
                style = MaterialTheme.typography.bodyMedium,
                modifier = Modifier.padding(start = 16.dp)
            )
        }
    }
}
