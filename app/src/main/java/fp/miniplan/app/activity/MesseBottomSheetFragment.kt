package fp.miniplan.app.activity

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import fp.miniplan.app.R
import fp.miniplan.app.activity.main.task.MesseBottomSheet
import fp.miniplan.app.module.viewmodel.SetupViewModel
import fp.miniplan.app.ui.theme.ComposeMaterialYou
import fp.miniplan.fragment.MesseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel


class MesseBottomSheetFragment(val messe: MesseFragment) : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                ComposeMaterialYou {
                    Column(modifier = Modifier.padding(horizontal = 8.dp)) {
                        MesseBottomSheet(messe)
                    }
                }
            }
        }
    }


    fun show(fragmentManager: FragmentManager) {
        show(fragmentManager, tag)
    }

    override fun getTheme(): Int {
        return R.style.CustomBottomSheetDialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        removeFragment()
        super.onDismiss(dialog)
    }

    private fun removeFragment() {
        this.parentFragmentManager.beginTransaction().remove(this).commit()
    }
}
