package fp.miniplan.app.job

import android.Manifest
import android.annotation.SuppressLint
import android.app.Notification
import android.app.PendingIntent
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.apollographql.apollo3.cache.normalized.FetchPolicy
import com.apollographql.apollo3.exception.ApolloException
import fe.android.preference.helper.PreferenceRepository
import fp.miniplan.GetUserMinistrantsTasksQuery
import fp.miniplan.app.BuildConfig
import fp.miniplan.app.R
import fp.miniplan.app.activity.main.MainActivity
import fp.miniplan.app.activity.main.task.TaskRepository
import fp.miniplan.app.module.preference.Preferences
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@SuppressLint("SpecifyJobSchedulerIdRange")
class TaskJobService : JobService(), KoinComponent {
    private val taskRepository by inject<TaskRepository>()
    private val preferences by inject<PreferenceRepository>()
    private val coroutineScope = CoroutineScope(Dispatchers.IO)

    override fun onStartJob(params: JobParameters?): Boolean {
        if (BuildConfig.DEBUG) Log.d("TaskJobService", "Started task job service")

        val notify = preferences.getBoolean(Preferences.notifyOnNewTasksAvailable)
        coroutineScope.launch {
            try {
                val newTasks = fetchTasksAndGetCount(FetchPolicy.NetworkOnly)

                if (notify) {
                    val oldTasks = fetchTasksAndGetCount(FetchPolicy.CacheOnly)
                    if (newTasks > oldTasks) {
                        sendNotification(newTasks - oldTasks)
                    }
                }
            } catch (e: ApolloException) {
                e.printStackTrace()
            }

            jobFinished(params, true)
        }

        return true
    }

    private suspend fun fetchTasksAndGetCount(fetchPolicy: FetchPolicy): Int {
        return getTaskAmount(taskRepository.fetchTasks(fetchPolicy = fetchPolicy))
    }

    private fun hasNotificationPermission(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ActivityCompat.checkSelfPermission(
                this@TaskJobService,
                Manifest.permission.POST_NOTIFICATIONS
            ) == PackageManager.PERMISSION_GRANTED
        } else true
    }

    @SuppressLint("MissingPermission")
    private fun sendNotification(newTasks: Int) {
        if (hasNotificationPermission()) {
            val notification = buildNotification(applicationContext, newTasks)
            val manager = NotificationManagerCompat.from(applicationContext)

            manager.notify(TASK_JOB_SERVICE_NOTIFICATION_ID, notification)
        } else {
            Log.w(
                "TaskJobService",
                "Tried to send notification for $newTasks, but no notification permission granted"
            )
        }
    }

    private fun getTaskAmount(data: GetUserMinistrantsTasksQuery.Data?): Int {
        return data?.UserMinistrant?.map { it.ministrantRel.tasksRel }?.size ?: 0
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        return false
    }

    companion object {
        const val TASK_JOB_SERVICE_NOTIFICATION = "TaskJobServiceNotification"
        const val TASK_JOB_SERVICE_NOTIFICATION_ID = 25345341

        fun buildNotification(applicationContext: Context, taskCounter: Int): Notification {
            return NotificationCompat.Builder(
                applicationContext, TASK_JOB_SERVICE_NOTIFICATION
            ).apply {
                setSmallIcon(R.drawable.ic_pray_white)
                setContentTitle(applicationContext.getString(R.string.new_tasks_available))
                setContentText(
                    applicationContext.getString(
                        R.string.new_tasks_amount_app,
                        taskCounter
                    )
                )
                priority = NotificationCompat.PRIORITY_DEFAULT
                setContentIntent(
                    PendingIntent.getActivity(
                        applicationContext,
                        0,
                        Intent(applicationContext, MainActivity::class.java),
                        PendingIntent.FLAG_IMMUTABLE
                    )
                )

                setAutoCancel(true)
            }.build()
        }
    }
}