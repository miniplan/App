import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties
import de.fayard.refreshVersions.core.versionFor

plugins {
    id("com.android.application")
    id("com.apollographql.apollo3")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-parcelize")
}

android {
    compileSdk = 34

    defaultConfig {
        applicationId = "fp.miniplan.app"
        minSdk = 24
        targetSdk = 33
        versionCode = 57
        versionName = "1.22.1"
        vectorDrawables.useSupportLibrary = true
        setProperty("archivesBaseName", "$applicationId-v$versionCode($versionName)")

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        renderscriptTargetApi = 18
        renderscriptSupportModeEnabled = true
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )

            val properties = gradleLocalProperties(rootDir)
            buildConfigField("String", "CRASH_LOGIN", properties["MINIPLAN_CRASH_LOGIN"].toString())
            buildConfigField(
                "String",
                "CRASH_PASSWORD",
                properties["MINIPLAN_CRASH_PASSWORD"].toString()
            )
            buildConfigField(
                "String",
                "CRASH_REPORT_ENDPOINT",
                "\"https://crash.miniapp.at/report\""
            )
            buildConfigField("String", "AUTH_HOST", "\"https://auth.miniapp.at\"")
            buildConfigField("String", "API_HOST", "\"https://data.miniapp.at/v1/graphql\"")
            buildConfigField("String", "PRIVACY_LINK", "\"https://miniapp.at/privacy\"")
            buildConfigField(
                "String",
                "SIGNIN_REGEX",
                "\"https:\\\\/\\\\/miniapp\\\\.at\\\\/signin\\\\/(.*)\""
            )

            resValue("string", "app_name", "Miniplan")
        }

        create("beta") {
//            isMinifyEnabled = true
//            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")

            val properties = gradleLocalProperties(rootDir)
            buildConfigField(
                "String",
                "CRASH_LOGIN",
                properties["MINIPLAN_BETA_CRASH_LOGIN"].toString()
            )
            buildConfigField(
                "String",
                "CRASH_PASSWORD",
                properties["MINIPLAN_BETA_CRASH_PASSWORD"].toString()
            )
            buildConfigField(
                "String",
                "CRASH_REPORT_ENDPOINT",
                "\"https://crash.beta.miniapp.at/report\""
            )
            buildConfigField("String", "AUTH_HOST", "\"https://auth.beta.miniapp.at\"")
            buildConfigField("String", "API_HOST", "\"https://data.beta.miniapp.at/v1/graphql\"")
            buildConfigField("String", "PRIVACY_LINK", "\"https://beta.miniapp.at/privacy\"")
            buildConfigField(
                "String",
                "SIGNIN_REGEX",
                "\"https:\\\\/\\\\/beta\\\\.miniapp\\\\.at\\\\/signin\\\\/(.*)\""
            )

            applicationIdSuffix = ".beta"
            resValue("string", "app_name", "Miniplan Beta")
        }

        debug {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
//            val properties = gradleLocalProperties(rootDir)
//            buildConfigField("String", "CRASH_LOGIN", properties["MINIPLAN_BETA_CRASH_LOGIN"].toString())
//            buildConfigField("String", "CRASH_PASSWORD", properties["MINIPLAN_BETA_CRASH_PASSWORD"].toString())
//            buildConfigField("String", "CRASH_REPORT_ENDPOINT", "\"https://crash.beta.miniapp.at/report\"")
//            buildConfigField("String", "AUTH_HOST", "\"https://auth.beta.miniapp.at\"")
//            buildConfigField("String", "API_HOST", "\"https://data.beta.miniapp.at/v1/graphql\"")
//            buildConfigField("String", "PRIVACY_LINK", "\"https://beta.miniapp.at/privacy\"")
//            buildConfigField("String", "SIGNIN_REGEX", "\"https:\\\\/\\\\/beta\\\\.miniapp\\\\.at\\\\/signin\\\\/(.*)\"")
            buildConfigField("String", "CRASH_LOGIN", "null")
            buildConfigField("String", "CRASH_PASSWORD", "null")
            buildConfigField("String", "CRASH_REPORT_ENDPOINT", "\"http://192.168.8.2:1929\"")
            buildConfigField("String", "AUTH_HOST", "\"http://192.168.8.2:2898\"")
            buildConfigField("String", "API_HOST", "\"http://192.168.8.2:8080/v1/graphql\"")
            buildConfigField("String", "PRIVACY_LINK", "\"http://192.168.8.2:2898/privacy\"")
            buildConfigField(
                "String",
                "SIGNIN_REGEX",
                "\"http:\\\\/\\\\/192\\\\.168\\\\.8\\\\.2:2898\\\\/signin\\\\/(.*)\""
            )

            applicationIdSuffix = ".debug"
            isDebuggable = true
            resValue("string", "app_name", "Miniplan Debug")
        }
    }

    packaging {
        resources {
            excludes += setOf("/META-INF/{AL2.0,LGPL2.1}", "META-INF/atomicfu.kotlin_module")
        }
    }

    java {
        toolchain {
            languageVersion.set(JavaLanguageVersion.of(17))
        }
    }

    buildFeatures {
        viewBinding = true
        compose = true
    }

    composeOptions {
        kotlinCompilerExtensionVersion = versionFor(AndroidX.compose.compiler)
    }

    namespace = "fp.miniplan.app"
}

apollo {
    service("service") {
        packageName.set("fp.miniplan")
    }
}

dependencies {
    implementation(Kotlin.stdlib)

    implementation(AndroidX.recyclerView)
    implementation(AndroidX.coordinatorLayout)
    implementation(AndroidX.core.ktx)
    implementation(AndroidX.appCompat)
    implementation(AndroidX.constraintLayout)
    implementation(AndroidX.navigation.fragmentKtx)
    implementation(AndroidX.navigation.uiKtx)
    implementation(AndroidX.preference.ktx)
    implementation(AndroidX.swipeRefreshLayout)
    implementation(AndroidX.work.runtimeKtx)

    implementation(AndroidX.activity.compose)
    implementation(AndroidX.compose.material3)
    implementation(AndroidX.compose.material.icons.extended)
    implementation(AndroidX.compose.compiler)
    implementation(AndroidX.compose.ui.toolingPreview)
    implementation(AndroidX.compose.material)
    implementation(AndroidX.compose.animation)
    implementation(AndroidX.compose.ui)

    implementation(libs.acra.http)
    implementation(libs.acra.advanced.scheduler)

    implementation(libs.legacy.support.v4)


    implementation(ApolloGraphQL.runtime)
    implementation(libs.apollo.normalized.cache.incubating)
    implementation(libs.apollo.normalized.cache.sqlite.incubating)
    implementation(libs.java.jwt)

    implementation(libs.libphonenumber)

    api(KotlinX.coroutines.core)
    api(KotlinX.coroutines.android)

    implementation(Square.retrofit2)
    implementation(Square.retrofit2.converter.gson)
    implementation(Square.retrofit2.converter.scalars)

    implementation(libs.gsonktextensions)
    implementation(libs.preference.helper)
    implementation(libs.preference.helper.compose)

    implementation(Google.accompanist.swipeRefresh)


    implementation(AndroidX.compose.ui.tooling)
    androidTestImplementation(AndroidX.compose.ui.testJunit4)
    implementation(Google.accompanist.appCompatTheme)


    implementation(Koin.android)
    implementation(Koin.compose)


    implementation(Google.android.material)

    implementation(libs.sdp.android)
    implementation(libs.gson)
    implementation(Square.okHttp3)
    implementation(libs.java.semver)
    implementation(libs.elevationimageview)
    implementation(libs.licenser)
    implementation(libs.android.joda)
    implementation(libs.io.noties.markwon.core)


    debugImplementation(AndroidX.compose.ui.testManifest)
    testImplementation(Testing.junit4)
    androidTestImplementation(AndroidX.test.ext.junit)
    androidTestImplementation(AndroidX.test.espresso.core)

}
