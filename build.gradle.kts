buildscript {
    repositories {
        maven(url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
        google()
        mavenCentral()
    }

    dependencies {
        classpath(Android.tools.build.gradlePlugin)
        classpath(Kotlin.gradlePlugin)
        classpath(libs.apollo.gradle.plugin)
    }
}


allprojects {
    repositories {
        google()
        mavenCentral()
        maven(url = "https://jitpack.io")
    }
}

tasks.create("clean", type = Delete::class) {
    delete(rootProject.buildDir)
}

